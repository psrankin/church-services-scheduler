<?php

namespace Entity;
use \Doctrine\Common\Collections\ArrayCollection;
use \Doctrine\ORM\Mapping;

/**
 * @Entity
 */
class Location {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the location
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\Church
	 * @ManyToOne(targetEntity="Church", inversedBy="Locations", fetch="EAGER")
	 * @JoinColumn(name="ChurchId", referencedColumnName="Id")
	 */
	protected $Church;
	public function getChurch() { return $this->Church; }
	public function setChurch($val) { $this->Church = $val; }
	
	/**
	 * @Column(length=50)
	 * @var string The title of this location
	 */
	protected $Title;
	public function getTitle() { return $this->Title; }
	public function setTitle($val) { $this->Title = $val; }
	
	/**
	 * @Column
	 * @var string The address of this location
	 */
	protected $Address;
	public function getAddress() { return $this->Address; }
	public function setAddress($val) { $this->Address = $val; }
	
	/**
	 * @Column
	 * @var string Any comments about this particular location
	 */
	protected $Comments;
	public function getComments() { return $this->Comments; }
	public function setComments($val) { $this->Comments = $val; }
	
    /**
	 * @var \Doctrine\Common\Collections\ArrayCollection
     * @OneToMany(targetEntity="Service", mappedBy="Location")
     **/
	protected $Services;
	public function getServices() { return $this->Services; }
	public function setServices($val) { $this->Services = $val; }
	
	public function __construct() {
		$this->Services = new \Doctrine\Common\Collections\ArrayCollection();
	}
}
