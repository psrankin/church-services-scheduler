<?php

namespace Entity;

/**
 * @Entity
 */
class Privilege {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @Column(length=50)
	 */
	protected $Name;
	public function getName() { return $this->Name; }
	public function setName($val) { $this->Name = $val; }
	
	/**
	 * @Column
	 */
	protected $Description;
	public function getDescription() { return $this->Description; }
	public function setDescription($val) { $this->Description = $val; }
	
    /**
     * @ManyToMany(targetEntity="Role", mappedBy="Privileges")
     **/
	protected $Roles;
	public function getRoles() { return $this->Roles; }
	public function setRoles($val) { $this->Roles = $val; }
	
	public function __construct() {
		$this->Roles = new \Doctrine\Common\Collections\ArrayCollection();
	}
}
