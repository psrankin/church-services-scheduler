<?php

namespace Entity;
use \Doctrine\Common\Collections\ArrayCollection;
use \Doctrine\ORM\Mapping;

/**
 * @Entity
 */
class Item {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the song
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\Church
	 * @ManyToOne(targetEntity="Church", inversedBy="Items", fetch="EAGER")
	 * @JoinColumn(name="ChurchId", referencedColumnName="Id")
	 */
	protected $Church;
	public function getChurch() { return $this->Church; }
	public function setChurch($val) { $this->Church = $val; }
	
	/**
	 * @var \Entity\Hymnal
	 * @ManyToOne(targetEntity="Hymnal", fetch="EAGER")
	 * @JoinColumn(name="HymnalId", referencedColumnName="Id")
	 */
	protected $Hymnal;
	public function getHymnal() { return $this->Hymnal; }
	public function setHymnal($val) { $this->Hymnal = $val; }
	
	/**
	 * @Column(length=20)
	 */
	protected $Type;
	public function getType() { return $this->Type; }
	public function setType($val) { $this->Type = $val; }
	
	/**
	 * @Column(length=100)
	 * @var string The title of the song
	 */
	protected $Title;
	public function getTitle() { return $this->Title; }
	public function setTitle($val) { $this->Title = $val; }
	
	/**
	 * @Column
	 * @var string The details for the song (performers, etc.)
	 */
	protected $Summary;
	public function getSummary() { return $this->Summary; }
	public function setSummary($val) { $this->Summary= $val; }
	
	/**
	 * @Column(length=10)
	 * @var string The length of the song in the format m:ss (e.g., 2:05 for two minutes five seconds)
	 */
	protected $Length;
	public function getLength() { return $this->Length; }
	public function setLength($val) { $this->Length = $val; }
	public function getLengthNormalized() {
		if ($this->Length !== '' && $this->Length !== null) {
			if (strpos($this->Length, ":") !== false) {
				return $this->Length;
			} else {
				return $this->Length . ":00";
			}
		} else {
			return '';
		}
	}
	
	/**
	 * @Column(length=20, name="`Key`")
	 * @var string The musical key(s) of the song
	 */
	protected $Key;
	public function getKey() { return $this->Key; }
	public function setKey($val) { $this->Key = $val; }
	
	/**
	 * @Column(length=10)
	 */
	protected $Page;
	public function getPage() { return $this->Page; }
	public function setPage($val) { $this->Page = $val; }
	
	/**
	 * @Column
	 * @var string HTML of any content associated with the song
	 */
	protected $Details;
	public function getDetails() { return $this->Details; }
	public function setDetails($val) { $this->Details = $val; }
	
    /**
	 * @var \Doctrine\Common\Collections\ArrayCollection
     * @OneToMany(targetEntity="ServiceItem", mappedBy="Item")
     **/
	protected $ServiceItems;
	public function getServiceItems() { return $this->ServiceItems; }
	public function setServiceItems($val) { $this->ServiceItems = $val; }
	
	/**
	 * Get the last time this item was performed at a specified location
	 * 
	 * @param integer $location_id The ID of the location for which to get the most recent performance
	 * @return \DateTime The date of the last performance, or NULL
	 */
	public function getLastPerformedAt($location_id) {
		$last_performed = null;
		
		// Loop processing all service items, finding the most recent performance at the specified location
		foreach ($this->getServiceItems() as $service_item) { /* @var $service_item \Entity\ServiceItem */
			if ($last_performed === null || ($last_performed < $service_item->getService()->getStart() && $service_item->getService()->getLocation() == $location_id)) {
				$last_performed = $service_item->getService()->getStart();
			}
		}
		
		return $last_performed;
	}
	
    /**
	 * @var \Doctrine\Common\Collections\ArrayCollection()
	 * 
     * @ManyToMany(targetEntity="Tag")
     * @JoinTable(name="ItemTag",
     *      joinColumns={@JoinColumn(name="ItemId", referencedColumnName="Id")},
     *      inverseJoinColumns={@JoinColumn(name="TagId", referencedColumnName="Id")}
     *      )
     **/
	protected $Tags;
	public function getTags() { return $this->Tags; }
	public function setTags($val) { $this->Tags = $val; }
	public function getTagTitles() {
		$tag_titles = array();
		foreach ($this->getTags() as $tag) { /* @var $tag Entity\Tag */
			array_push($tag_titles, $tag->getTitle());
		}
		return $tag_titles;
	}
	
	/**
	 * Get a full title, including page number and key, if possible
	 * 
	 * @return string The full title
	 */
	public function getTitleFull() {
		$full_title = $this->getTitle();
		if ($this->getPage())
			$full_title = "#{$this->getPage()}: $full_title";
		if ($this->getKey())
			$full_title .= " ({$this->getKey()})";
		return $full_title;
	}
	
	/**
	 * Get the sort-friendly title (i.e., song title first, not hymn number)
	 */
	public function getTitleFullSortable() {
		$full_title = $this->getTitle();
		if ($this->getKey())	$full_title .= " ({$this->getKey()})";
		if ($this->getPage())	$full_title .= ", #" . $this->getPage();
		return $full_title;
	}
	
	public function toJsonReady() {
		return array(
			"Id"				=> $this->getId(),
			"ChurchId"			=> $this->getChurch() ? $this->getChurch()->getId() : null,
			"HymnalId"			=> $this->getHymnal() ? $this->getHymnal()->getId() : null,
			"Type"				=> $this->getType(),
			"Title"				=> $this->getTitle(),
			"Summary"			=> $this->getSummary(),
			"Length"			=> $this->getLength(),
			"LengthNormalized"	=> $this->getLengthNormalized(),
			"Key"				=> $this->getKey(),
			"Page"				=> $this->getPage(),
			"Details"			=> $this->getDetails(),
			"Tags"				=> array_map(function($x) { return $x->toJsonReady(); }, $this->getTags()->toArray())
		);
	}
	
	public function __construct() {
		$this->Tags = new \Doctrine\Common\Collections\ArrayCollection();
	}
}
