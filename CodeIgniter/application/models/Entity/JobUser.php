<?php

namespace Entity;

/**
 * @Entity
 */
class JobUser {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\Job
	 * @ManyToOne(targetEntity="Job", inversedBy="JobUsers", fetch="EAGER")
	 * @JoinColumn(name="JobId", referencedColumnName="Id")
	 */
	protected $Job;
	public function getJob() { return $this->Job; }
	public function setJob($val) { $this->Job = $val; }
	
	/**
	 * @var \Entity\User
	 * @ManyToOne(targetEntity="User", inversedBy="JobUsers", fetch="EAGER")
	 * @JoinColumn(name="UserId", referencedColumnName="Id")
	 */
	protected $User;
	public function getUser() { return $this->User; }
	public function setUser($val) { $this->User = $val; }
	
	/**
	 * @Column(length=50)
	 */
	protected $Status;
	public function getStatus() { return $this->Status; }
	public function setStatus($val) { $this->Status = $val; }
}
