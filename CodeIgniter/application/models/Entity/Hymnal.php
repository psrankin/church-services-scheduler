<?php

namespace Entity;

/**
 * @Entity
 */
class Hymnal {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\Church
	 * @ManyToOne(targetEntity="Church", inversedBy="Hymnals", fetch="EAGER")
	 * @JoinColumn(name="ChurchId", referencedColumnName="Id")
	 */
	protected $Church;
	public function getChurch() { return $this->Church; }
	public function setChurch($val) { $this->Church = $val; }
	
	/**
	 * @Column(length=50)
	 */
	protected $Title;
	public function getTitle() { return $this->Title; }
	public function setTitle($val) { $this->Title = $val; }
	
	/**
	 * @Column
	 */
	protected $Description;
	public function getDescription() { return $this->Description; }
	public function setDescription($val) { $this->Description = $val; }
	
	public function __construct() {
		$this->HymnalItems = new \Doctrine\Common\Collections\ArrayCollection();
	}
}
