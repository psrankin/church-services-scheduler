<?php

namespace Entity;

/**
 * @Entity
 */
class LinkTicket {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\User
	 * @ManyToOne(targetEntity="User", fetch="EAGER")
	 * @JoinColumn(name="UserId", referencedColumnName="Id")
	 */
	protected $User;
	public function getUser() { return $this->User; }
	public function setUser($val) { $this->User = $val; }
	
	/**
	 * @var \Entity\Church
	 * @ManyToOne(targetEntity="Church", fetch="EAGER")
	 * @JoinColumn(name="ChurchId", referencedColumnName="Id")
	 */
	protected $Church;
	public function getChurch() { return $this->Church; }
	public function setChurch($val) { $this->Church = $val; }
	
	/**
	 * @Column(length=255)
	 */
	protected $Ticket;
	public function getTicket() { return $this->Ticket; }
	public function setTicket($val) { $this->Ticket = $val; }
	public function generateTicket($num_chars = 7) { $this->setTicket($this->random_str($num_chars)); }
	
	/**
	 * Generate a random string, using a cryptographically secure 
	 * pseudorandom number generator (random_int)
	 * 
	 * For PHP 7, random_int is a PHP core function
	 * For PHP 5.x, depends on https://github.com/paragonie/random_compat
	 * 
	 * @param int $length      How many characters do we want?
	 * @param string $keyspace A string of all possible characters
	 *                         to select from
	 * @return string
	 */
	private function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
		$str = '';
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$str .= $keyspace[random_int(0, $max)];
		}
		return $str;
	}
	
	/**
	 * @Column(type="datetime")
	 * @var \DateTime
	 */
	protected $Expires;
	public function getExpires() { return $this->Expires; }
	public function setExpires($val) { $this->Expires = $val; }
	public function IsExpired() {
		return $this->getExpires() < new \DateTime();
	}
	
	/**
	 * @Column(type="boolean")
	 */
	protected $IsActive;
	public function getIsActive() { return $this->IsActive; }
	public function setIsActive($val) { $this->IsActive = $val; }
	
	/**
	 * Verify that this ticket is valid
	 * 
	 * @param string $action The action that is being requested for this ticket
	 * (to ensure that one ticket cannot be used to accomplish something for which
	 * it was not intended)
	 */
	public function Verify($action) {
		return $action === $this->getAction() && !$this->IsExpired() && $this->getIsActive();
	}
	
	/**
	 * @Column(length=255)
	 */
	protected $Action;
	public function getAction() { return $this->Action; }
	public function setAction($val) { $this->Action = $val; }
	
	/**
	 * @Column(length=255)
	 */
	protected $Subject;
	public function getSubject() { return $this->Subject; }
	public function setSubject($val) { $this->Subject = $val; }
	
	/**
	 * @Column
	 */
	protected $Message;
	public function getMessage() { return $this->Message; }
	public function setMessage($val) { $this->Message = $val; }
	public function getLink() { return site_url(trim($this->getAction(), "/") . "/{$this->getTicket()}/"); }
	public function getFullMessage() {
		$link = $this->getLink();
		return "{$this->getMessage()}\n\n$link";
	}
	
	/**
	 * @Column
	 */
	protected $Data;
	public function getData() { return $this->Data; }
	public function setData($val) { $this->Data = $val; }
	
	public function sendEmail($address = null) {
		$ci =& get_instance(); /* @var $ci \CI_Controller */
		$ci->load->library("email");
		
		$ci->email
			->from("noreply@christianprogrammers.com", "Church Services Scheduler")
			->to($address ? $address : $this->getUser()->getEmail())
			->subject($this->getSubject())
			->message($this->getFullMessage())
			->send();
	}
}
