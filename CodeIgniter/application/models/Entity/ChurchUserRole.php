<?php

namespace Entity;

/**
 * @Entity
 */
class ChurchUserRole {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\Church
	 * @ManyToOne(targetEntity="Church", inversedBy="UserRoles", fetch="EAGER")
	 * @JoinColumn(name="ChurchId", referencedColumnName="Id")
	 */
	protected $Church;
	public function getChurch() { return $this->Church; }
	public function setChurch($val) { $this->Church = $val; }
	
	/**
	 * @var \Entity\User
	 * @ManyToOne(targetEntity="User", inversedBy="ChurchRoles", fetch="EAGER")
	 * @JoinColumn(name="UserId", referencedColumnName="Id")
	 */
	protected $User;
	public function getUser() { return $this->User; }
	public function setUser($val) { $this->User = $val; }
	
	/**
	 * @var \Entity\Role
	 * @ManyToOne(targetEntity="Role", fetch="EAGER")
	 * @JoinColumn(name="RoleId", referencedColumnName="Id")
	 */
	protected $Role;
	public function getRole() { return $this->Role; }
	public function setRole($val) { $this->Role = $val; }
}
