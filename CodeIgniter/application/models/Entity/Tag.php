<?php

namespace Entity;
use \Doctrine\Common\Collections\ArrayCollection;
use \Doctrine\ORM\Mapping;

/**
 * @Entity
 */
class Tag {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the tag
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\Church
	 * @ManyToOne(targetEntity="Church", inversedBy="Tags", fetch="EAGER")
	 * @JoinColumn(name="ChurchId", referencedColumnName="Id")
	 */
	protected $Church;
	public function getChurch() { return $this->Church; }
	public function setChurch($val) { $this->Church = $val; }
	
	/**
	 * @Column(length=50)
	 * @var string The title of this tag
	 */
	protected $Title;
	public function getTitle() { return $this->Title; }
	public function setTitle($val) { $this->Title = $val; }
	
	/**
	 * @Column(length=20)
	 */
	protected $Category;
	public function getCategory() { return $this->Category; }
	public function setCategory($val) { $this->Category = $val; }
	
	
	/**
	 * @Column
	 * @var string Detail about when this tag applies
	 */
	protected $Details;
	public function getDetails() { return $this->Details; }
	public function setDetails($val) { $this->Details = $val; }
	
	/**
	 * @ManyToMany(targetEntity="Item", mappedBy="Tags")
	 * @var \Doctrine\Common\Collections\ArrayCollection()
	 * @OrderBy({"Title" = "ASC"})
	 */
	protected $Items;
	public function getItems() { return $this->Items; }
	public function setItems($val) { $this->Items = $val; }
	
	/**
	 * Convert this tag to an object ready to be serialized as JSON
	 */
	public function toJsonReady() {
		return array(
			"Id"		=> $this->Id,
			"Title"		=> $this->Title,
			"Details"	=> $this->Details
		);
	}
	
	public function __construct() {
		$this->Items = new \Doctrine\Common\Collections\ArrayCollection();
	}
}
