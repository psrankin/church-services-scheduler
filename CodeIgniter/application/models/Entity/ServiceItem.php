<?php

namespace Entity;
use \Doctrine\Common\Collections\ArrayCollection;
use \Doctrine\ORM\Mapping;

/**
 * @Entity
 */
class ServiceItem {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\Service
	 * @ManyToOne(targetEntity="Service", inversedBy="ServiceItems", fetch="EAGER")
	 * @JoinColumn(name="ServiceId", referencedColumnName="Id")
	 */
	protected $Service;
	public function getService() { return $this->Service; }
	public function setService($val) { $this->Service = $val; }
	
	/**
	 * @var \Entity\Item
	 * @ManyToOne(targetEntity="Item", inversedBy="ServiceItems", fetch="EAGER")
	 * @JoinColumn(name="ItemId", referencedColumnName="Id")
	 */
	protected $Item;
	public function getItem() { return $this->Item; }
	public function setItem($val) { $this->Item = $val; }
	
	/**
	 * @Column(type="integer")
	 */
	protected $Weight;
	public function getWeight() { return $this->Weight; }
	public function setWeight($val) { $this->Weight = $val; }
	
	/**
	 * Convert this object to a JSON-ready array
	 */
	public function toJsonReady() {
		return array(
			"Id"			=> $this->getId(),
			"ServiceId"		=> $this->getService()->getId(),
			"Item"			=> $this->getItem() ? $this->getItem()->toJsonReady() : null,
			"Weight"		=> $this->getWeight()
		);
	}
}


