<?php

namespace Entity;

/**
 * @Entity
 */
class Job {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\Church
	 * @ManyToOne(targetEntity="Church", inversedBy="Jobs", fetch="EAGER")
	 * @JoinColumn(name="ChurchId", referencedColumnName="Id")
	 */
	protected $Church;
	public function getChurch() { return $this->Church; }
	public function setChurch($val) { $this->Church = $val; }
	
	/**
	 * @Column(length=20)
	 */
	protected $Category;
	public function getCategory() { return $this->Category; }
	public function setCategory($val) { $this->Category = $val; }
	
	/**
	 * @Column(length=50)
	 */
	protected $Title;
	public function getTitle() { return $this->Title; }
	public function setTitle($val) { $this->Title = $val; }
	
	/**
	 * @Column
	 */
	protected $Description;
	public function getDescription() { return $this->Description; }
	public function setDescription($val) { $this->Description = $val; }
}
