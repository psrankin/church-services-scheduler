<?php 

namespace Entity;

/** @Entity */
class User {
	/**
	 * @Id @Column(type="integer")
	 * @GeneratedValue
	 * @var int The ID of this page
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @Column(length=255)
	 */
	protected $PasswordHashed;
	public function getPasswordHashed() { return $this->PasswordHashed; }
	public function setPasswordHashed($val) { $this->PasswordHashed = $val; }
	
	/**
	 * @Column
	 */
	protected $FirstName;
	public function getFirstName() { return $this->FirstName; }
	public function setFirstName($val) { $this->FirstName = $val; }
	
	/**
	 * @Column
	 */
	protected $LastName;
	public function getLastName() { return $this->LastName; }
	public function setLastName($val) { $this->LastName = $val; }
	
	public function getFullName() {
		return $this->getFirstName() . " " . $this->getLastName();
	}
	
	/**
	 * @Column
	 */
	protected $Email;
	public function getEmail() { return $this->Email; }
	public function setEmail($val) { $this->Email = $val; }
	
	/**
	 * @Column(type="boolean")
	 */
	protected $IsSuperAdmin;
	public function getIsSuperAdmin() { return $this->IsSuperAdmin; }
	public function setIsSuperAdmin($val) { $this->IsSuperAdmin = $val; }
	
    /**
	 * @var \Doctrine\Common\Collections\ArrayCollection
     * @OneToMany(targetEntity="ChurchUserRole", mappedBy="User")
     **/
	protected $ChurchRoles;
	public function getChurchRoles() { return $this->ChurchRoles; }
	public function setChurchRoles($val) { $this->ChurchRoles = $val; }
	public function getRoles($church_id) {
		$roles = array();
		
		// Loop processing each role, filtering and mapping accordingly
		foreach ($this->getChurchRoles() as $church_user_role) { /* @var $church_user_role ChurchUserRole */
			if ($church_user_role->getChurch()->getId() == $church_id)
				array_push($roles, $church_user_role->getRole());
		}
		
		return $roles;
	}
	/**
	 * @return Role
	 */
	public function getCurrentChurchRole() {
		$ci =& get_instance(); /* @var $ci \CI_Controller */
		$roles = $this->getRoles($ci->authorization->getCurrentChurchId());
		if (count($roles) === 0) return null;
		return $roles[0];
	}
	public function getChurches() {
		$churches = array();
		
		// Loop processing each role, adding the appropriate church
		foreach ($this->getChurchRoles() as $church_user_role) { /* @var $church_user_role ChurchUserRole */
			if (array_search($church_user_role->getChurch(), $churches) === false)
				array_push($churches, $church_user_role->getChurch());
		}
		
		return $churches;
	}
	
	public function __construct() {
		$this->ChurchRoles = new \Doctrine\Common\Collections\ArrayCollection();
	}
}


