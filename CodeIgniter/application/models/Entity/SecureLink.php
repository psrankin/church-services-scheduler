<?php

namespace Entity;

/**
 * @Entity
 */
class SecureLink {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @Column(length=255)
	 */
	protected $Token;
	public function getToken() { return $this->Token; }
	public function setToken($val) { $this->Token = $val; }
	
	/**
	 * Generate a random token for this secure link
	 */
	public function generateToken() {
		
	}
	
	/**
	 * @var \Entity\User
	 * @ManyToOne(targetEntity="User", fetch="EAGER")
	 * @JoinColumn(name="UserId", referencedColumnName="Id")
	 */
	protected $User;
	public function getUser() { return $this->User; }
	public function setUser($val) { $this->User = $val; }
	
	/**
	 * @Column(length=255)
	 */
	protected $Action;
	public function getAction() { return $this->Action; }
	public function setAction($val) { $this->Action = $val; }
	
	/**
	 * @Column
	 */
	protected $Data;
	public function getData() { return $this->Data; }
	public function setData($val) { $this->Data = $val; }
}
