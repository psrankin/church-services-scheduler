<?php

namespace Entity;
use \Doctrine\Common\Collections\ArrayCollection;
use \Doctrine\ORM\Mapping;

/**
 * @Entity
 */
class Service {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\Church
	 * @ManyToOne(targetEntity="Church", inversedBy="Services", fetch="EAGER")
	 * @JoinColumn(name="ChurchId", referencedColumnName="Id")
	 */
	protected $Church;
	public function getChurch() { return $this->Church; }
	public function setChurch($val) { $this->Church = $val; }
	
	/**
	 * @var \Entity\Location 
	 * @ManyToOne(targetEntity="Location", inversedBy="Services", fetch="EAGER")
	 * @JoinColumn(name="LocationId", referencedColumnName="Id")
	 */
	protected $Location;
	public function getLocation() { return $this->Location; }
	public function setLocation($val) { $this->Location = $val; }
	
	/**
	 * @Column
	 * @var string Any details about this service
	 */
	protected $Summary;
	public function getSummary() { return $this->Summary; }
	public function setSummary($val) { $this->Summary = $val; }
	
	/**
	 * @Column(type="datetime", name="`Start`")
	 * @var \DateTime The time of the event
	 */
	protected $Start;
	public function getStart() { return $this->Start; }
	public function setStart($val) { $this->Start = $val; }
	
	/**
	 * @Column(length=20)
	 */
	protected $Visibility;
	public function getVisibility() { return $this->Visibility; }
	public function setVisibility($val) { $this->Visibility = $val; }
	
    /**
	 * @var \Doctrine\Common\Collections\ArrayCollection
     * @OneToMany(targetEntity="ServiceItem", mappedBy="Service")
	 * @OrderBy({"Weight" = "ASC"})
     **/
	protected $ServiceItems;
	public function getServiceItems() { return $this->ServiceItems; }
	public function setServiceItems($val) { $this->ServiceItems = $val; }
	public function getServiceItemsJsonReady() {
		return array_map(function($x) { return $x->toJsonReady(); }, $this->getServiceItems()->toArray());
	}
	
	public function getTotalLength() {
		$total_minutes = 0;
		$total_seconds = 0;
		
		// Loop processing each service item, tallying the total minutes/seconds
		foreach ($this->getServiceItems() as $service_item) { /* @var $service_item \Entity\ServiceItem */
			$normalized_length = $service_item->getItem()->getLengthNormalized();
			if ($normalized_length) {
				$components = explode(":", $normalized_length);
				$total_minutes += intval($components[0]);
				$total_seconds += intval($components[1]);
			}
		}
		
		$total_minutes += floor($total_seconds / 60);
		$total_seconds %= 60;
		
		if ($total_minutes >= 0 || $total_seconds >= 0) {
			return "{$total_minutes}:" . str_pad($total_seconds, 2, "0", STR_PAD_LEFT);
		} else {
			return "";
		}
	}
	
	public function __construct() {
		$this->ServiceItems = new \Doctrine\Common\Collections\ArrayCollection();
	}
}


