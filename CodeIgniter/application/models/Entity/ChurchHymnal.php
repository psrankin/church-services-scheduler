<?php

namespace Entity;

/**
 * @Entity
 */
class ChurchHymnal {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\Church
	 * @ManyToOne(targetEntity="Church", fetch="EAGER")
	 * @JoinColumn(name="ChurchId", referencedColumnName="Id")
	 */
	protected $Church;
	public function getChurch() { return $this->Church; }
	public function setChurch($val) { $this->Church = $val; }
	
	/**
	 * @var \Entity\Hymnal
	 * @ManyToOne(targetEntity="Hymnal", fetch="EAGER")
	 * @JoinColumn(name="HymnalId", referencedColumnName="Id")
	 */
	protected $Hymnal;
	public function getHymnal() { return $this->Hymnal; }
	public function setHymnal($val) { $this->Hymnal = $val; }
}
