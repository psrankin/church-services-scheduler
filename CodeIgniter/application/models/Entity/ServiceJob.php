<?php

namespace Entity;
use \Doctrine\Common\Collections\ArrayCollection;
use \Doctrine\ORM\Mapping;

/**
 * @Entity
 */
class ServiceJob {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\Service
	 * @ManyToOne(targetEntity="Service", inversedBy="Items", fetch="EAGER")
	 * @JoinColumn(name="ServiceId", referencedColumnName="Id")
	 */
	protected $Service;
	public function getService() { return $this->Service; }
	public function setService($val) { $this->Service = $val; }
	
	/**
	 * @var \Entity\Job
	 * @ManyToOne(targetEntity="Job", inversedBy="ServiceJobs", fetch="EAGER")
	 * @JoinColumn(name="JobId", referencedColumnName="Id")
	 */
	protected $Job;
	public function getJob() { return $this->Job; }
	public function setJob($val) { $this->Job = $val; }
	
	/**
	 * @Column(type="integer")
	 */
	protected $QtyDesired;
	public function getQtyDesired() { return $this->QtyDesired; }
	public function setQtyDesired($val) { $this->QtyDesired = $val; }
	
	/**
	 * @Column(type="integer")
	 */
	protected $QtyScheduled;
	public function getQtyScheduled() { return $this->QtyScheduled; }
	public function setQtyScheduled($val) { $this->QtyScheduled = $val; }
}


