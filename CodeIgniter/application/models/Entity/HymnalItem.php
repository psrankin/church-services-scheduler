<?php

namespace Entity;
use \Doctrine\Common\Collections\ArrayCollection;
use \Doctrine\ORM\Mapping;

/**
 * @Entity
 */
class HymnalItem {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the song
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\Hymnal
	 * @ManyToOne(targetEntity="Hymnal", inversedBy="HymnalItems", fetch="EAGER")
	 * @JoinColumn(name="HymnalId", referencedColumnName="Id")
	 */
	protected $Hymnal;
	public function getHymnal() { return $this->Hymnal; }
	public function setHymnal($val) { $this->Hymnal = $val; }
	
	/**
	 * @var \Entity\Item
	 * @ManyToOne(targetEntity="Item", inversedBy="HymnalItems", fetch="EAGER")
	 * @JoinColumn(name="ItemId", referencedColumnName="Id")
	 */
	protected $Item;
	public function getItem() { return $this->Item; }
	public function setItem($val) { $this->Item = $val; }
	
	/**
	 * @Column(length=20)
	 */
	protected $Page;
	public function getPage() { return $this->Page; }
	public function setPage($val) { $this->Page = $val; }
	
	/**
	 * @Column(length=10)
	 */
	protected $Key;
	public function getKey() { return $this->Key; }
	public function setKey($val) { $this->Key = $val; }
	
    /**
	 * @var \Doctrine\Common\Collections\ArrayCollection
     * @OneToMany(targetEntity="ServiceItem", mappedBy="HymnalItem")
     **/
	protected $ServiceItems;
	public function getServiceItems() { return $this->ServiceItems; }
	public function setServiceItems($val) { $this->ServiceItems = $val; }
	
	public function toJsonReady($primary_entity = false) {
		return array(
			"Id"		=> $this->getId(),
			"HymnalId"	=> $this->getHymnal()->getId(),
			"Item"		=> $this->getItem()->toJsonReady(),
			"Page"		=> $this->getPage(),
			"Key"		=> $this->getKey()
		);
	}
	
	public function __construct() {
		
	}
}
