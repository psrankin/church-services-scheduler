<?php

namespace Entity;

/**
 * @Entity
 */
class Role {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @Column(length=50)
	 */
	protected $Name;
	public function getName() { return $this->Name; }
	public function setName($val) { $this->Name = $val; }
	
	/**
	 * @Column
	 */
	protected $Description;
	public function getDescription() { return $this->Description; }
	public function setDescription($val) { $this->Description = $val; }
	
	/**
	 * @Column(type="integer")
	 */
	protected $Weight;
	public function getWeight() { return $this->Weight; }
	public function setWeight($val) { $this->Weight = $val; }
	
    /**
     * @ManyToMany(targetEntity="Privilege", inversedBy="Roles")
     * @JoinTable(name="RolePrivilege",
     *      joinColumns={@JoinColumn(name="RoleId", referencedColumnName="Id")},
     *      inverseJoinColumns={@JoinColumn(name="PrivilegeId", referencedColumnName="Id")}
     *      )
     **/
	protected $Privileges;
	public function getPrivileges() { return $this->Privileges; }
	public function setPrivileges($val) { $this->Privileges = $val; }
	
	public function __construct() {
		$this->Roles = new \Doctrine\Common\Collections\ArrayCollection();
	}
}
