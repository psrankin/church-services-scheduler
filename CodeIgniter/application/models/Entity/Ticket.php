<?php 

namespace Entity;

/** @Entity */
class Ticket {
	/**
	 * @Id @Column(length=50)
	 * @var string The ID of this page
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
    /**
	 * @var \Entity\User
	 * 
     * @ManyToOne(targetEntity="User", fetch="EAGER")
     * @JoinColumn(name="UserId", referencedColumnName="Id")
     **/
	protected $User;
	public function getUser() { return $this->User; }
	public function setUser($val) { $this->User = $val; }
	
	/**
	 * @Column
	 */
	protected $UserIP;
	public function getUserIP() { return $this->UserIP; }
	public function setUserIP($val) { $this->UserIP = $val; }
	
	/**
	 * @Column(type="datetime")
	 */
	protected $Expires;
	public function getExpires() { return $this->Expires; }
	public function setExpires($val) { $this->Expires = $val; }
	
	/**
	 * @Column(type="boolean")
	 */
	protected $Revoked;
	public function getRevoked() { return $this->Revoked; }
	public function setRevoked($val) { $this->Revoked = $val; }
}


