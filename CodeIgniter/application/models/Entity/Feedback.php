<?php

namespace Entity;

/**
 * @Entity
 */
class Feedback {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @var \Entity\User
	 * @ManyToOne(targetEntity="User", fetch="EAGER")
	 * @JoinColumn(name="UserId", referencedColumnName="Id")
	 */
	protected $User;
	public function getUser() { return $this->User; }
	public function setUser($val) { $this->User = $val; }
	
	/**
	 * @Column(type="datetime")
	 */
	protected $Submitted;
	public function getSubmitted() { return $this->Submitted; }
	public function setSubmitted($val) { $this->Submitted = $val; }
	
	/**
	 * @Column(length=255)
	 */
	protected $Url;
	public function getUrl() { return $this->Url; }
	public function setUrl($val) { $this->Url = $val; }
	
	/**
	 * @Column(length=20)
	 */
	protected $Version;
	public function getVersion() { return $this->Version; }
	public function setVersion($val) { $this->Version = $val; }
	
	/**
	 * @Column
	 */
	protected $Content;
	public function getContent() { return $this->Content; }
	public function setContent($val) { $this->Content = $val; }
}



