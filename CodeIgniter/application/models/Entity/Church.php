<?php

namespace Entity;

/**
 * @Entity
 */
class Church {
	/**
	 * @Column @Id @GeneratedValue
	 * @var type The ID of the event
	 */
	protected $Id;
	public function getId() { return $this->Id; }
	public function setId($val) { $this->Id = $val; }
	
	/**
	 * @Column(length=50)
	 */
	protected $Title;
	public function getTitle() { return $this->Title; }
	public function setTitle($val) { $this->Title = $val; }
	
	/**
	 * @Column
	 */
	protected $Description;
	public function getDescription() { return $this->Description; }
	public function setDescription($val) { $this->Description = $val; }
	
    /**
	 * @var \Doctrine\Common\Collections\ArrayCollection
     * @OneToMany(targetEntity="ChurchUserRole", mappedBy="User")
     **/
	protected $UserRoles;
	public function getUserRoles() { return $this->UserRoles; }
	public function setUserRoles($val) { $this->UserRoles = $val; }
	
    /**
	 * @var \Doctrine\Common\Collections\ArrayCollection
     * @OneToMany(targetEntity="Location", mappedBy="Church")
     **/
	protected $Locations;
	public function getLocations() { return $this->Locations; }
	public function setLocations($val) { $this->Locations = $val; }
	
    /**
	 * @var \Doctrine\Common\Collections\ArrayCollection
     * @OneToMany(targetEntity="Service", mappedBy="Church")
     **/
	protected $Services;
	public function getServices() { return $this->Services; }
	public function setServices($val) { $this->Services = $val; }
	
    /**
	 * @var \Doctrine\Common\Collections\ArrayCollection
     * @OneToMany(targetEntity="Hymnal", mappedBy="Church")
     **/
	protected $Hymnals;
	public function getHymnals() { return $this->Hymnals; }
	public function setHymnals($val) { $this->Hymnals = $val; }
	
    /**
	 * @var \Doctrine\Common\Collections\ArrayCollection
     * @OneToMany(targetEntity="Item", mappedBy="Church")
	 * @OrderBy({"Title" = "ASC"})
     **/
	protected $Items;
	public function getItems() { return $this->Items; }
	public function setItems($val) { $this->Items = $val; }
	
	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection
     * @OneToMany(targetEntity="Tag", mappedBy="Church")
     **/
	protected $Tags;
	public function getTags() { return $this->Tags; }
	public function setTags($val) { $this->Tags = $val; }
	
	public function __construct() {
		$this->UserRoles	= new \Doctrine\Common\Collections\ArrayCollection();
		$this->Locations	= new \Doctrine\Common\Collections\ArrayCollection();
		$this->Services		= new \Doctrine\Common\Collections\ArrayCollection();
		$this->Items		= new \Doctrine\Common\Collections\ArrayCollection();
	}
}


