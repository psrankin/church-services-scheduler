<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Church extends CI_Controller {
	
	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		
	}
	
	public function edit() {
		$this->authorization->RequiresPrivilege("church_edit");
		$this->load->library(["form_validation", "crud"]);
		$church = $this->authorization->getCurrentChurch();
		
		$this->form_validation->set_rules("Title", "Title", "required|max_length[50]");
		
		if ($this->form_validation->run()) {
			$church->setTitle($_POST['Title']);
			$church->setDescription($_POST['Description']);
			$this->doctrine->em->flush();
			
			redirect();
		} else {
			$this->load->view("layout/header");
			$this->load->view("church/edit", ["church" => $church]);
			$this->load->view("layout/footer");
		}
	}
}
