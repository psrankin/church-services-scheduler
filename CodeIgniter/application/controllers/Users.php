<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	public function __construct() {
		parent::__construct();
		
		$this->authorization->RequiresPrivilege("users_manage");
	}
	
	public function index() {
		$users = $this->doctrine->em->createQuery("Select u, cur, r From \Entity\User u Join u.ChurchRoles cur Join cur.Role r " . 
			"Where Exists (Select cur2 From \Entity\ChurchUserRole cur2 Where cur2.User = u And cur2.Church = ?0)")
			->execute([$this->authorization->getCurrentChurchPartial()]);
		$this->load->library("crud");
		
		$this->load->view("layout/header");
		$this->load->view("users/index", ["users" => $users]);
		$this->load->view("layout/footer");
	}
	
	public function add() {
		$this->load->library("form_validation");
		
		$this->form_validation->set_rules("FirstName", "First Name", "required|max_length[50]");
		$this->form_validation->set_rules("LastName", "Last Name", "required|max_length[50]");
		$this->form_validation->set_rules("Email", "E-mail Address", "required|max_length[50]|valid_email");
		$this->form_validation->set_rules("RoleId", "Role", "required|in_list[1,2,3,4]");
		
		if ($this->form_validation->run()) {
			// First, check whether the user already exists.  If not, create the user.
			$user = $this->doctrine->em->getRepository("\Entity\User")->findOneBy(["Email" => $_POST['Email']]);
			if (!$user) {
				$user = new Entity\User();
				$this->doctrine->em->persist($user);
				$user->setFirstName($_POST['FirstName']);
				$user->setLastName($_POST['LastName']);
				$user->setEmail($_POST['Email']);
				$user->setIsSuperAdmin(false);
				
				// Create the confirmation ticket (for the user's confirmation e-mail)
				$this->load->helper("security");
				$link_ticket = create_link_ticket("signup/set-password", "New Church Scheduler Account",
						"You have been added to your church's service scheduling software.  Please click the link below to set your password.");
				$link_ticket->setUser($user);
				$link_ticket->setChurch($this->authorization->getCurrentChurchPartial());
				$link_ticket->sendEmail();
			}
			
			// Add the user role
			$role = $this->doctrine->em->find("Entity\Role", $_POST['RoleId']);
			$this->doctrine->em->getRepository("Entity\ChurchUserRole");
			$church_user_role = new Entity\ChurchUserRole();
			$this->doctrine->em->persist($church_user_role);
			$church_user_role->setChurch($this->authorization->getCurrentChurchPartial());
			$church_user_role->setUser($user);
			$church_user_role->setRole($role);
			
			$this->doctrine->em->flush();
			
			$this->load->helper("url");
			redirect("/users/");
		} else {
			$this->load->view("layout/header");
			$this->load->view("users/add", ["roles" => $this->_get_roles()]);
			$this->load->view("layout/footer");
		}
	}
	
	/**
	 * 
	 * @param integer $id The user whose account to edit
	 */
	public function edit($id) {
		$this->_verify_access_for($id);
		
		$this->load->library("form_validation");
		$this->form_validation->set_rules("FirstName", "First Name", "required|max_length[50]");
		$this->form_validation->set_rules("LastName", "Last Name", "required|max_length[50]");
		$this->form_validation->set_rules("Email", "E-mail Address", "required|max_length[50]|valid_email");
		$this->form_validation->set_rules("RoleId", "Role", "required|in_list[1,2,3,4]");
		$this->form_validation->set_rules("Password", "Password", "max_length[50]");
		$this->form_validation->set_rules("ConfirmPassword", "Confirm Password", "matches[Password]");
		
		$user = $this->doctrine->em->find("Entity\User", $id); /* @var $user Entity\User */
		$this->load->library("crud");
		
		if ($this->form_validation->run()) {
			$this->crud->Bind($user, ["FirstName", "LastName", "Email"]);
			
			// Check whether the role must be updated
			if ($_POST['RoleId'] != $user->getCurrentChurchRole()->getId()) {
				$church = $this->authorization->getCurrentChurchPartial();
				$church_user_role = $this->doctrine->em->getRepository("Entity\ChurchUserRole")
					->findOneBy(["Church" => $church, "User" => $user]); /* @var $church_user_role Entity\ChurchUserRole */
				$church_user_role->setRole($this->doctrine->em->getPartialReference("Entity\Role", $_POST['RoleId']));
			}
			
			// Check whether the password must be updated
			if (isset($_POST['ChangePassword']) && $_POST['ChangePassword']) {
				$user->setPasswordHashed(password_hash($_POST['Password'], PASSWORD_BCRYPT));
			}
			
			// Store the changes
			$this->doctrine->em->flush();
			
			$this->load->helper("url");
			redirect("/users/");
		} else {
			$this->load->view("layout/header");
			$this->load->view("users/edit", ["user" => $user, "roles" => $this->_get_roles()]);
			$this->load->view("layout/footer");
		}
	}
	
	/**
	 * Remove the specified user from having access to this church's account
	 * 
	 * @param integer $id The ID of the user to remove from this church account access
	 */
	public function remove($id) {
		$church = $this->authorization->getCurrentChurchPartial();
		$user   = $this->doctrine->em->getPartialReference("Entity\User", $id);
		
		$this->doctrine->em->createQuery("Delete Entity\ChurchUserRole cur Where cur.Church = ?0 And cur.User = ?1")->execute([$church, $user]);
		
		$this->load->helper("url");
		redirect("/users/");
	}
	
	private function _verify_access_for($id) {
		$this->load->library("doctrine");
		$church = $this->authorization->getCurrentChurchPartial();
		$user   = $this->doctrine->em->find("Entity\User", $id);
		$church_user_role = $this->doctrine->em->getRepository("Entity\ChurchUserRole")->findOneBy(["Church" => $church, "User" => $user]);
		if (!$church_user_role) {
			$this->authorization->AccessDenied();
		}
	}
	
	private function _get_roles() {
		return $this->doctrine->em->createQuery("Select r From \Entity\Role r Order By r.Weight")->execute();
	}
	
	public function login() {
		
	}
}
