<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_items extends CI_Controller {
	
	public function index() {
		
	}
	
	/**
	 * Allow the user to add a new item to a service
	 * 
	 * @param integer $service_id The ID of the service to which to add this new item
	 */
	public function add($service_id) {
		$service = $this->doctrine->em->find("Entity\Service", $service_id); /* @var $service Entity\Service */
		$this->doctrine->em->getRepository("Entity\ServiceItem");
		
		$items = $this->doctrine->em->createQuery(
			"Select i From Entity\Item i Join i.Church c Left Join i.ServiceItems si Left Join si.Service s " .
			"Where c.Id = ?0 And Not Exists (Select si2 From Entity\ServiceItem si2 Join si2.Service s2 Join si2.Item i2 Where i2.Id = i.Id And s2.Id = ?1) " . 
			"Order By i.Title Asc")
			->execute([$this->authorization->getCurrentChurchId(), $service_id]);
		
		$this->load->helper("form");
		$this->load->view("layout/header");
		$this->load->view("service-items/add", [
			"service"	=> $service,
			"items"		=> $items
		]);
		$this->load->view("layout/footer");
	}
	
	/**
	 * Edit the specified service item
	 * 
	 * @param integer $id The ID of the service item to edit
	 */
	public function edit($id) {
		$service_item = $this->doctrine->em->find("Entity\ServiceItem", $id); /* @var $service_item \Entity\ServiceItem */
		$this->authorization->RequiresPrivilege("services_edit", $service_item->getService()->getChurch()->getId());
		$this->load->helper("form");
		$this->load->library("crud");
		
		$this->load->library("form_validation");
		$this->form_validation
			->set_rules("Type", "Type", "required|in_list[Congregational,Special,Sermon,Other]")
			->set_rules("Title", "Title", "required|max_length[50]")
			->set_rules("Length", "Length", "max_length[10]|regex_match[/^([0-9]{1,2}(:[0-9]{2})?)?$/]")
			->set_rules("Key", "Key", "max_length[20]")
			->set_rules("Page", "Page", "max_length[10]");
		if ($this->form_validation->run()) {
			$this->crud->Bind($service_item->getItem(), ["Type", "Title", "Length", "Key", "Page", "Summary", "Details"]);
			$this->doctrine->em->flush();
			
			redirect("services/edit/{$service_item->getService()->getId()}");
		} else {
			$this->load->view("layout/header");
			$this->load->view("service-items/edit", ["service_item" => $service_item]);
			$this->load->view("layout/footer");
		}
	}
	
	/**
	 * Allow the user to view a particular service item
	 * 
	 * @param integer $id the ID of the service item to view
	 */
	public function view($id) {
		$service_item = $this->doctrine->em->find("Entity\ServiceItem", $id); /* @var $service_item \Entity\ServiceItem */
		$this->authorization->RequiresPrivilege("services_view", $service_item->getService()->getChurch()->getId());
		$this->load->library("crud");
		
		$this->load->view("layout/header");
		$this->load->view("service-items/view", ["service_item" => $service_item]);
		$this->load->view("layout/footer");
	}
	
	/**
	 * Set the order for service items
	 * 
	 * Include a POST variable with the key: "ServiceItemIds" as a JSON-encoded array
	 * of service item IDs
	 * 
	 * @param integer $service_id The ID of the service whose items' order to set
	 */
	public function reorder($service_id) {
		$service			= $this->doctrine->em->find("Entity\Service", $service_id); /* @var $service Entity\Service */
		$service_item_ids	= json_decode($_POST['ServiceItemIds']);
		
		// Loop processing each service item, setting its weight
		foreach ($service->getServiceItems() as $service_item) { /* @var $service_item Entity\ServiceItem */
			$service_item->setWeight(array_search($service_item->getId(), $service_item_ids));
		}
		
		$this->doctrine->em->flush();
	}
	
	/**
	 * Move the relative position of the specified service item up
	 * 
	 * @param integer $id The ID of the service item to move up
	 */
	public function move_down($id) {
		$service_item		= $this->doctrine->em->find("Entity\ServiceItem", $id); /* @var $service_item Entity\ServiceItem */
		$service_items		= $service_item->getService()->getServiceItems()->toArray();
		$service_item_ids	= array_map(function($x) { return $x->getId(); }, $service_items);
		
		// Find the index of the specified item
		$index = array_search($id, $service_item_ids);
		if (count($service_item_ids) > $index + 1) {
			$temp						= $service_items[$index];
			$service_items[$index]		= $service_items[$index + 1];
			$service_items[$index + 1]	= $temp;
		}
		
		// Loop processing each service item, setting its relative weight
		for ($i = 0; $i < count($service_items); $i++) { /* @var $service_item Entity\ServiceItem */
			$service_items[$i]->setWeight($i);
		}
		
		$this->doctrine->em->flush();
		redirect("services/edit/{$service_item->getService()->getId()}/");
	}
	
	/**
	 * Move the relative position of the specified service item down
	 * 
	 * @param integer $id The ID of the service item to move down
	 */
	public function move_up($id) {
		$service_item		= $this->doctrine->em->find("Entity\ServiceItem", $id); /* @var $service_item Entity\ServiceItem */
		$service_items		= $service_item->getService()->getServiceItems()->toArray();
		$service_item_ids	= array_map(function($x) { return $x->getId(); }, $service_items);
		
		// Find the index of the specified item
		$index = array_search($id, $service_item_ids);
		if ($index > 0) {
			$temp						= $service_items[$index];
			$service_items[$index]		= $service_items[$index - 1];
			$service_items[$index - 1]	= $temp;
		}
		
		// Loop processing each service item, setting its relative weight
		for ($i = 0; $i < count($service_items); $i++) { /* @var $service_item Entity\ServiceItem */
			$service_items[$i]->setWeight($i);
		}
		
		$this->doctrine->em->flush();
		redirect("services/edit/{$service_item->getService()->getId()}/");
	}
	
	/**
	 * Remove a service item (e.g., remove item from service)
	 * 
	 * @param integer $id The ID of the service item to delete
	 */
	public function delete($id) {
		$id = intval($id);
		$service_item = $this->doctrine->em->find("Entity\ServiceItem", $id); /* @var $service_item Entity\ServiceItem */
		$this->authorization->RequiresPrivilege("services_edit", $service_item->getService()->getChurch()->getId());
		
		$this->doctrine->em->remove($service_item);
		$this->doctrine->em->flush();
		
		redirect("services/edit/{$service_item->getService()->getId()}");
	}
	
	/**
	 * Add an existing item to the service
	 * 
	 * @param integer $service_id The ID of the service to which to add a new item
	 * @param integer $item_id The ID of the item to add to the service
	 */
	public function add_existing($service_id, $item_id) {
		// Load the service and item
		$service_id = intval($service_id);
		$item_id	= intval($item_id);
		$service	= $this->doctrine->em->find("Entity\Service", $service_id); /* @var $service Entity\Service */
		$item		= $this->doctrine->em->find("Entity\Item", $item_id); /* @var $item Entity\Item */
		
		// Verify user access
		$this->authorization->RequiresPrivilege("services_edit", $service->getChurch()->getId());
		if ($item->getChurch() && $item->getChurch()->getId() !== $service->getChurch()->getId())
			$this->authorization->AccessDenied();
		
		// Add the item to the service
		$this->doctrine->em->getRepository("Entity\ServiceItem");
		$service_item = new Entity\ServiceItem();
		$this->doctrine->em->persist($service_item);
		$service_item->setService($service);
		$service_item->setItem($item);
		$service_item->setWeight($service->getServiceItems()->count());
		$this->doctrine->em->flush();
		
		$this->load->helper("url");
		redirect("services/edit/" . $service->getId());
	}
	
	
	public function add_hymn($service_id) {
		$service = $this->doctrine->em->find("Entity\Service", $service_id); /* @var $service Entity\Service */
		$this->authorization->RequiresPrivilege("services_edit", $service->getChurch()->getId());
		
		$this->load->helper("form");
		$this->load->view("layout/header");
		$this->load->view("service-items/hymn/add", ["service" => $service]);
		$this->load->view("layout/footer");
	}
}
