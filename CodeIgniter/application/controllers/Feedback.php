<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->authorization->RequiresLogin();
	}
	
	/**
	 * Index Page for this controller.
	 */
	public function index() {
		$this->load->library("form_validation");
		$this->load->library("user_agent");
		$url = isset($_POST['Url']) ? $_POST['Url'] : $this->agent->referrer();
		
		$this->form_validation->set_rules("Content", "Content", "required");
		
		if ($this->form_validation->run()) {
			$this->doctrine->em->getRepository("Entity\Feedback");
			$feedback = new Entity\Feedback();
			$this->doctrine->em->persist($feedback);
			$feedback->setUser($this->doctrine->em->getPartialReference("Entity\User", $this->authentication->GetUserId()));
			$feedback->setContent($_POST['Content']);
			$feedback->setUrl($_POST['Url']);
			$feedback->setVersion(APP_VERSION);
			$this->doctrine->em->flush();
			
			$this->load->view("layout/header");
			$this->load->view("feedback/success", ["redirect" => $url]);
			$this->load->view("layout/footer");
		} else {
			$this->load->library("crud");
			$this->load->view("layout/header");
			$this->load->view("feedback/index", ["url" => $url]);
			$this->load->view("layout/footer");
		}
	}
	
	public function view($limit = 500) {
		$this->authorization->RequiresSuperAdmin();
		
		$this->load->library("crud");
		$feedback = $this->doctrine->em->getRepository("Entity\Feedback")->findBy([], ["Submitted" => "Desc"], $limit);
		
		$this->load->view("layout/header");
		$this->load->view("feedback/view", ["feedback" => $feedback]);
		$this->load->view("layout/footer");
	}
}
