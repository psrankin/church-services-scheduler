<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {
	public function index() {
		$this->load->library("form_validation");
		
		$this->form_validation->set_rules("ChurchTitle", "Church Name", "required|max_length[50]");
		$this->form_validation->set_rules("Email", "Email", "required|valid_email");
		$this->form_validation->set_rules("FirstName", "First Name", "required|max_length[50]");
		$this->form_validation->set_rules("LastName", "Last Name", "required|max_length[50]");
		$this->form_validation->set_rules("Password", "Password", "required");
		$this->form_validation->set_rules("ConfirmPassword", "Confirm Password", "matches[Password]");
		
		if ($this->form_validation->run()) {
			// Create the verification ticket and send the e-mail
			$this->load->helper("security");
			$link_ticket = create_link_ticket(
				"signup/verify",
				"Account Verification",
				"Please click this link to verify your new account.",
				array(
					"ChurchTitle"		=> $_POST['ChurchTitle'],
					"Email"				=> $_POST['Email'],
					"FirstName"			=> $_POST['FirstName'],
					"LastName"			=> $_POST['LastName'],
					"PasswordHashed"	=> password_hash($_POST['Password'], PASSWORD_BCRYPT)
				),
				"+2 days");
			
			$this->doctrine->em->flush();
			$link_ticket->sendEmail($_POST['Email']);
			
			redirect("signup/success");
		} else {
			$this->load->library("crud");
			$this->load->view("layout/public/header");
			$this->load->view("signup/index");
			$this->load->view("layout/public/footer");
		}
	}
	
	public function success() {
		$this->load->view("layout/isolated/header");
		$this->load->view("signup/success");
		$this->load->view("layout/isolated/footer");
	}
	
	/**
	 * This user has been added by a church administrator.  The account already exists,
	 * but without a password.  Allow the user to set a password.
	 * 
	 * @param string $ticket The ticket
	 */
	public function set_password($ticket) {
		$this->load->helper("security");
		$link_ticket = verify_ticket($ticket, "signup/set-password");
		
		$this->load->library("form_validation");
		$this->form_validation->set_rules("Password", "Password", "matches[ConfirmPassword]");
		$this->form_validation->set_rules("ConfirmPassword", "ConfirmPassword", "required");
		
		if ($this->form_validation->run()) {
			$user = $link_ticket->getUser();
			$user->setPasswordHashed(password_hash($_POST['Password'], PASSWORD_BCRYPT));
			$this->doctrine->em->flush();
			
			redirect();
		} else {
			$this->load->view("layout/isolated/header");
			$this->load->view("signup/set_password");
			$this->load->view("layout/isolated/footer");
		}
	}
	
	public function verify($ticket) {
		$link_ticket = $this->doctrine->em->getRepository("Entity\LinkTicket")->findOneBy(["Ticket" => $ticket]); /* @var $link_ticket Entity\LinkTicket */
		if (!$link_ticket->Verify("signup/verify")) {
			show_error("The link is either invalid or is expired.");
		}
		$data = unserialize($link_ticket->getData());
		
		// First, create the church
		$this->doctrine->em->getRepository("Entity\Church");
		$church = new Entity\Church();
		$this->doctrine->em->persist($church);
		$church->setTitle($data['ChurchTitle']);
		$church->setDescription("");
		
		// Second, create the user account
		$this->doctrine->em->getRepository("Entity\User");
		$user = new Entity\User();
		$this->doctrine->em->persist($user);
		$user->setEmail($data['Email']);
		$user->setFirstName($data['FirstName']);
		$user->setLastName($data['LastName']);
		$user->setPasswordHashed($data['PasswordHashed']);
		$user->setIsSuperAdmin(false);
		
		// Third, link the user to the church as an administrator
		$this->doctrine->em->getRepository("Entity\ChurchUserRole");
		$church_user_role = new Entity\ChurchUserRole();
		$this->doctrine->em->persist($church_user_role);
		$church_user_role->setChurch($church);
		$church_user_role->setUser($user);
		$church_user_role->setRole($this->doctrine->em->getRepository("Entity\Role")->findOneBy(["Name" => "Administrator"]));
		
		// Set the link ticket as inactive
		$link_ticket->setIsActive(false);
		$this->doctrine->em->flush();
		
		// Finally, display the success message to the user
		redirect('signup/verify-success');
	}
	
	public function verify_success() {
		$this->load->view("layout/header");
		$this->load->view("signup/verify");
		$this->load->view("layout/footer");
	}
}
