<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_account extends CI_Controller {
	
	private $user_id;
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
		$this->authorization->RequiresLogin();
		$user = $this->doctrine->em->find("Entity\User", $this->authentication->GetUserId()); /* @var $user Entity\User */
		$this->load->library(["crud", "form_validation"]);
		
		$this->form_validation->set_rules("FirstName", "First Name", "required|max_length[50]");
		$this->form_validation->set_rules("LastName", "Last Name", "required|max_length[50]");
		
		if ($this->form_validation->run()) {
			$user->setFirstName($_POST['FirstName']);
			$user->setLastName($_POST['LastName']);
			$this->doctrine->em->flush();
			
			redirect();
		} else {
			$this->load->view("layout/header");
			$this->load->view("my-account/index", ["user" => $user]);
			$this->load->view("layout/footer");
		}
	}
	
	public function login() {
		$this->load->library("form_validation");
		
		$this->form_validation->set_rules("Username", "Username", "required");
		$this->form_validation->set_rules("Password", "Password", array("required", array("password_callable", function($password) {
			$this->user_id = $this->authentication->TryAuthenticate($this->input->post("Username"), $password);
			return ($this->user_id ? true : false);
		})));
		$this->form_validation->set_message("password_callable", "The username/password combination you entered is not correct.");
		
		if ($this->form_validation->run()) {
			$this->load->library("authentication");
			$this->authentication->GrantAccessAs($this->user_id);
			$this->load->helper("url");
			if (isset($_SESSION['login_redirect'])) {
				// Make sure the timestamp is recent (to prevent "old" redirects which confuse the user)
				$now	= (new DateTime())->getTimestamp();
				$then	= $_SESSION['login_redirect']['Timestamp'];
				$diff	= abs($now - $then) / 60;
				if ($diff < 1) {
					$redirect = $_SESSION['login_redirect']['Url'];
				} else {
					$redirect = "";
				}
				unset($_SESSION['login_redirect']);
				
				redirect($redirect);
			} else {
				redirect("/");
			}
		} else {
			$this->load->view("layout/isolated/header");
			$this->load->view("my-account/login");
			$this->load->view("layout/isolated/footer");
		}
	}
	
	public function church_profile() {
		$this->authorization->RequiresPrivilege("church_manage");
		$church = $this->authorization->getCurrentChurch();
		$this->load->library(["crud", "form_validation"]);
		
		if ($this->form_validation->run()) {
			$this->crud->Bind($church, ["Title", "Description"]);
			$this->doctrine->em->flush();
			
			redirect();
		} else {
			$this->load->view("layout/header");
			$this->load->view("my-account/church-profile", ["church" => $church]);
			$this->load->view("layout/footer");
		}
	}
	
	/**
	 * Allow the user to reset his or her password from a secure link
	 * 
	 * @param string $ticket The ticket to verify the link
	 */
	public function reset_password_link($ticket) {
		$link_ticket = $this->doctrine->em->getRepository("Entity\LinkTicket")->findOneBy(["Ticket" => $ticket]); /* @var $link_ticket Entity\LinkTicket */
		if (!$link_ticket->Verify("my-account/reset-password-link")) {
			show_error("The link you clicked is invalid or expired.");
		} else {
			$this->load->library("form_validation");
			$this->form_validation->set_rules("Password", "Password", "required");
			$this->form_validation->set_rules("ConfirmPassword", "Confirm Password", "matches[Password]");
			
			if ($this->form_validation->run()) {
				$link_ticket->getUser()->setPasswordHashed(password_hash($_POST['Password'], PASSWORD_BCRYPT));
				$link_ticket->setIsActive(false);
				$this->doctrine->em->flush();
				
				redirect("my-account/login");
			} else {
				$this->load->view("layout/isolated/header");
				$this->load->view("my-account/reset-password-link");
				$this->load->view("layout/isolated/footer");
			}
		}
	}
	
	/**
	 * Allow the user to reset a forgotten password
	 */
	public function reset_password() {
		$this->load->library("form_validation");
		$this->form_validation->set_rules("Email", "Email", "required|valid_email");
		
		if ($this->form_validation->run()) {
			$user = $this->doctrine->em->getRepository("Entity\User")->findOneBy(["Email" => $_POST['Email']]);
			if ($user) {
				$this->load->helper("security");
				$link_ticket = create_link_ticket(
					"my-account/reset-password-link",
					"Reset Password",
					"Please use the following link to reset your password.");
				$link_ticket->setUser($user);
				
				$this->doctrine->em->flush();
				$link_ticket->sendEmail();
			}
			
			$this->load->view("layout/isolated/header");
			$this->load->view("my-account/reset-password-success");
			$this->load->view("layout/isolated/footer");
		} else {
			$this->load->view("layout/isolated/header");
			$this->load->view("my-account/reset-password");
			$this->load->view("layout/isolated/footer");
		}
	}
	
	public function change_password() {
		$this->authorization->RequiresLogin();
		$this->load->library(["form_validation", "crud"]);
		
		$this->form_validation->set_rules("Password", "Password", "required");
		$this->form_validation->set_rules("ConfirmPassword", "Confirm Password", "matches[Password]");
		
		if ($this->form_validation->run()) {
			$user = $this->doctrine->em->find("Entity\User", $this->authentication->GetUserId()); /* @var $user Entity\User */
			$user->setPasswordHashed(password_hash($_POST['Password'], PASSWORD_BCRYPT));
			$this->doctrine->em->flush();
			
			$this->load->view("layout/header");
			$this->load->view("my-account/change-password-success");
			$this->load->view("layout/footer");
		} else {
			$this->load->view("layout/header");
			$this->load->view("my-account/change-password");
			$this->load->view("layout/footer");
		}
	}
	
	public function logout() {
		$this->authentication->LogOut();
		redirect("/my-account/login");
	}
	
	
}
