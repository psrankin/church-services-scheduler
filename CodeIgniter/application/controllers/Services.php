<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {
	
	public function index() {
		$this->authorization->RequiresPrivilege("services_view");
		
		$upcoming_services = $this->doctrine->em->createQuery("Select s From Entity\Service s Join s.Church c Where c.Id = ?1 And s.Start >= ?0 Order By s.Start Asc")
			->execute([new DateTime(), $this->authorization->getCurrentChurchId()]);
		
		$this->load->library("Crud");
		$this->load->view("layout/header");
		$this->load->view("services/index", ["upcoming_services" => $upcoming_services]);
		$this->load->view("layout/footer");
	}
	
	/**
	 * Share a service with others
	 * 
	 * @param integer $id The ID of the service to share
	 */
	public function share($id) {
		$id = intval($id);
		$service = $this->doctrine->em->find("Entity\Service", $id); /* @var $service Entity\Service */
		$this->authorization->RequiresPrivilege("services_edit", $service->getChurch()->getId());
		
		// Verify that link tokens are created for this service, and if not, create them
		$ticket_full		= $this->doctrine->em->getRepository("Entity\LinkTicket")->findOneBy(['Action' => "services/view/$id"]);
		$ticket_summary		= $this->doctrine->em->getRepository("Entity\LinkTicket")->findOneBy(['Action' => "services/view-summary/$id"]);
		
		if (!$ticket_full || !$ticket_summary) {
			if ($ticket_full) $this->doctrine->em->remove($ticket_full);
			if ($ticket_summary) $this->doctrine->em->remove($ticket_summary);
			
			$this->load->helper("security");
			$ticket_full = create_link_ticket("services/view/$id", "You have been invited to view a service", 
				"You have been invited to view the service for {$service->getStart()->format("F j g:ia")}.", [], "+90 days");
			$ticket_summary = create_link_ticket("services/view-summary/$id", "You have been invited to view a service", 
				"You have been invited to view the service for {$service->getStart()->format("F j g:ia")}.", [], "+90 days");
			$this->doctrine->em->flush();
		}
		
		$this->load->view("layout/header");
		$this->load->view("services/share", [
			"ticket_full" => $ticket_full,
			"ticket_summary" => $ticket_summary,
			"service" => $service]);
		$this->load->view("layout/footer");
	}
	
	public function add() {
		$this->authorization->RequiresPrivilege("services_add");
		$this->load->library(["form_validation", "crud"]);
		$this->_set_validation();
		
		if ($this->form_validation->run()) {
			$this->doctrine->em->getRepository("Entity\Service");
			$service = new Entity\Service();
			$this->doctrine->em->persist($service);
			$service->setChurch($this->authorization->getCurrentChurch());
			
			// Location is an optional field.
			if ($_POST['LocationId'] != -1) {
				$location = $this->doctrine->em->find("Entity\Location", $_POST['LocationId']); /* @var $location Entity\Location */
				if ($location->getChurch()->getId() != $this->authorization->getCurrentChurchId())
					$this->authorization->AccessDenied(); // Hacker is trying to add a location from another church
				$service->setLocation($location);
			}
			
			$start = new DateTime();
			$start->setTimestamp(strtotime($_POST['StartDate'] . " " . $_POST['StartTime']));
			$service->setStart($start);
			$service->setSummary($_POST['Summary']);
			$service->setVisibility("Private");
			
			$this->doctrine->em->flush();
			redirect("services/edit/" . $service->getId());
		} else {
			$this->load->view("layout/header");
			$this->load->view("services/add");
			$this->load->view("layout/footer");
		}
	}
	
	/**
	 * Set validation rules for add/edit screens
	 */
	private function _set_validation() {
		$this->form_validation->set_rules("StartDate", "Date", array(
			'required',
			array('format_callable', function($x) { return strtotime($x) !== false; })), 
			array('format_callable' => "Please be sure the Date field is in a valid format (like Jan 1, 2015, for example)."));
		$this->form_validation->set_rules("StartTime", "Time", array(
			'required', 
			array('format_callable', function($x) { return strtotime($x) !== false; })), 
			array('format_callable' => "Please be sure the Time field is in a valid format (like 6pm, or 11:30am, for example)."));
	}
	
	public function edit_master($id) {
		$service = $this->doctrine->em->find("Entity\Service", $id); /* @var $service Entity\Service */
		$this->authorization->RequiresPrivilege("services_edit", $service->getChurch()->getId());
		$this->load->library(["crud", "form_validation"]);
		
		$this->_set_validation();
		
		if ($this->form_validation->run()) {
			// Location is an optional field.  If it is filled in, verify security (to prevent cross-church info hacks)
			if ($_POST['LocationId'] != -1) {
				$location = $this->doctrine->em->find("Entity\Location", $_POST['LocationId']); /* @var $location Entity\Location */
				if ($location->getChurch()->getId() !== $service->getChurch()->getId())
					$this->authorization->AccessDenied(); // Hacker is trying to add a location from another church
				$service->setLocation($location);
			} else {
				$service->setLocation(null);
			}
			
			$start = new DateTime();
			$start->setTimestamp(strtotime($_POST['StartDate'] . " " . $_POST['StartTime']));
			$service->setStart($start);
			$service->setSummary($_POST['Summary']);
			
			$this->doctrine->em->flush();
			redirect("services/edit/" . $service->getId());
		} else {
			$this->load->view("layout/header");
			$this->load->view("services/edit-master", ["service" => $service]);
			$this->load->view("layout/footer");
		}
	}
	
	public function edit($id) {
		$service = $this->doctrine->em->find("Entity\Service", $id); /* @var $service Entity\Service */
		$this->authorization->RequiresPrivilege("services_edit", $service->getChurch()->getId());
		
		$this->load->view("layout/header");
		$this->load->view("services/edit", ["service" => $service]);
		$this->load->view("layout/footer");
	}
	
	public function view($id, $ticket = null) {
		$id = intval($id);
		$service = $this->doctrine->em->find("Entity\Service", $id); /* @var $service Entity\Service */
		if ($ticket) {
			$this->load->helper("security");
			verify_ticket($ticket, "services/view/$id");
		} else {
			$this->authorization->RequiresPrivilege("services_view", $service->getChurch()->getId());
		}
		
		if ($this->authentication->IsLoggedIn()) {
			$this->load->view("layout/header");
			$this->load->view("services/view", ["service" => $service]);
			$this->load->view("layout/footer");
		} else {
			$this->load->view("layout/public/header");
			$this->load->view("services/view", ["service" => $service]);
			$this->load->view("layout/public/footer");
		}
	}
	
	public function view_summary($id, $ticket = null) {
		$id = intval($id);
		$service = $this->doctrine->em->find("Entity\Service", $id); /* @var $service Entity\Service */
		if ($ticket) {
			$this->load->helper("security");
			verify_ticket($ticket, "services/view-summary/$id");
		} else {
			$this->authorization->RequiresPrivilege("services_view_summary", $service->getChurch()->getId());
		}
		
		if ($this->authentication->IsLoggedIn()) {
			$this->load->view("layout/header");
			$this->load->view("services/view-summary", ["service" => $service]);
			$this->load->view("layout/footer");
		} else {
			$this->load->view("layout/public/header");
			$this->load->view("services/view-summary", ["service" => $service]);
			$this->load->view("layout/public/footer");
		}
	}
	
	/**
	 * Delete an entire service
	 * 
	 * @param integer $id The ID of the service to delete
	 */
	public function delete($id) {
		$service = $this->doctrine->em->find("Entity\Service", $id); /* @var $service Entity\Service */
		$this->authorization->RequiresPrivilege("services_edit", $service->getChurch()->getId());
		
		// Loop processing all service items, removing them
		foreach ($service->getServiceItems() as $service_item) {
			$this->doctrine->em->remove($service_item);
		}
		$this->doctrine->em->remove($service);
		$this->doctrine->em->flush();
		
		redirect("services/");
	}
	
	/**
	 * Get the items in this service, ordered by weight
	 * 
	 * @param integer $id The ID of the service whose items to retrieve
	 * @return string A JSON-encoded array of ServiceItem objects
	 */
	public function items($id) {
		$service = $this->doctrine->em->find("Entity\Service", $id); /* @var $service Entity\Service */
		$this->authorization->RequiresPrivilege("services_view", $service->getChurch()->getId());
		
		$this->output->append_output(json_encode(array_map(function($si) {
			return $si->toJsonReady();
		}, $service->getServiceItems()->toArray())));
	}
	
	/**
	 * Allow the user to view archived services
	 */
	public function archive() {
		$this->authorization->RequiresPrivilege("services_view");
		
		$services = $this->doctrine->em->createQuery(
			"Select s From Entity\Service s Where s.Start < ?0 Order By s.Start Desc")
			->execute([new \DateTime()]);
		
		$this->load->library("crud");
		$this->load->view("layout/header");
		$this->load->view("services/archive", ["services" => $services]);
		$this->load->view("layout/footer");
	}
}
