<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Locations extends CI_Controller {
	
	public function index()
	{
		$this->load->library("doctrine");
		$locations = $this->doctrine->em->createQuery("Select l From Entity\Location l Order By l.Title Asc")->execute();
		
		$this->load->view("layout/header");
		$this->load->view("locations/index", ["locations" => $locations]);
		$this->load->view("layout/footer");
	}
	
	/**
	 * Delete an event
	 * 
	 * @param integer $id The ID of the event to delete
	 * @param string $confirm "confirm"
	 */
	public function delete($id) {
		
	}
	
	public function add() {
		$this->load->library(["doctrine", "crud"]);
		$this->doctrine->em->getRepository("Entity\Location");
		$location = new \Entity\Location();
		$this->doctrine->em->persist($location);
		
		$this->_setValidation();
		if ($this->form_validation->run()) {
			$this->crud->Bind($location, ["Title", "Address", "Comments"]);
			$location->setChurch($this->authorization->getCurrentChurchPartial());
			$this->doctrine->em->flush();
			
			$this->load->helper("url");
			redirect("/locations/");
		} else {
			$this->load->view("layout/header");
			$this->load->view("locations/edit", ["location" => $location]);
			$this->load->view("layout/footer");
		}
	}
	
	public function view($id) {
		$this->load->library("doctrine");
		$location = $this->doctrine->em->find("Entity\Location", $id);
		
		$this->load->view("layout/header");
		$this->load->view("locations/view", ["location" => $location]);
		$this->load->view("layout/footer");
	}
	
	public function edit($id) {
		$this->load->library("doctrine");
		$location = $this->doctrine->em->find("Entity\Location", $id); /* @var $event \Entity\Event */
		$this->_setValidation();
		
		if ($this->form_validation->run()) {
			$this->load->library("crud");
			$this->crud->Bind($location, ["Title", "Address", "Comments"]);
			$this->doctrine->em->flush();
			
			$this->load->helper("url");
			redirect("/locations/view/$id/");
		} else {
			$this->load->view("layout/header");
			$this->load->view("locations/edit", ["location" => $location]);
			$this->load->view("layout/footer");
		}
	}
	
	private function _setValidation() {
		$this->load->library("form_validation");
		$this->form_validation->set_rules("Title", "Title", "required|max_length[50]");
	}
}
