<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends CI_Controller {
	
	public function index() {
		$this->authorization->RequiresPrivilege("items_view");
		$items = $this->authorization->getCurrentChurch()->getItems();
		
		$this->load->library("crud");
		$this->load->view("layout/header");
		$this->load->view("items/index", ["items" => $items]);
		$this->load->view("layout/footer");
	}
	
	/**
	 * Get all of the items visible to the current church
	 */
	public function all() {
		// Display all valid items
		$items = $this->doctrine->em->createQuery("Select i From Entity\Item i Where i.Church = ?0 Or i.Church Is Null")
			->execute([$this->authorization->getCurrentChurchPartial()]);
		
		$this->output->append_output(json_encode(array_map(function($i) { 
			return $i->toJsonReady();
		}, $items)));
	}
	
	public function view($id) {
		$item = $this->doctrine->em->find("Entity\Item", $id); /* @var $item Entity\Item */
		if (!$item->getChurch()) {
			$this->authorization->RequiresLogin();
		} else {
			$this->authorization->RequiresPrivilege("items_view", $item->getChurch()->getId());
		}
		
		$this->load->library("crud");
		$this->load->view("layout/header");
		$this->load->view("items/view", ["item" => $item]);
		$this->load->view("layout/footer");
	}
	
	/**
	 * Add a new item, optionally adding it to a particular service
	 * 
	 * @param type $service_id
	 */
	public function add($service_id = null) {
		$this->authorization->RequiresPrivilege("services_edit", $this->authorization->getCurrentChurchId());
		
		$this->load->library("crud");
		$this->doctrine->em->getRepository("Entity\Item");
		$item = new Entity\Item();
		
		// Special case: If forwarded from another screen, the validation thinks it
		// should run because $_POST is set.  In this case, unset it.
		if ($_POST['Title'] && !isset($_POST['Type'])) {
			$item->setTitle ($_POST['Title']);
			unset($_POST['Title']);
		}
		
		$this->_set_validation();
		if ($this->form_validation->run()) {
			$this->doctrine->em->persist($item);
			$this->crud->Bind($item, ["Title", "Type", "Key", "Length", "Page", "Summary", "Details"]);
			
			// If the service ID is set, then bind this new item to the specified service
			if ($service_id !== null) {
				$service = $this->doctrine->em->find("Entity\Service", $service_id); /* @var $service Entity\Service */
				$this->authorization->RequiresPrivilege("services_edit", $service->getChurch()->getId());
				$this->doctrine->em->getRepository("Entity\ServiceItem");
				$service_item = new \Entity\ServiceItem();
				$this->doctrine->em->persist($service_item);
				$service_item->setItem($item);
				$service_item->setService($service);
				$service_item->setWeight(99999);
				
				$item->setChurch($service->getChurch());
			} else {
				$item->setChurch($this->authorization->getCurrentChurchPartial());
			}
			
			$this->doctrine->em->flush();
			if ($service_id !== null) {
				redirect("services/edit/$service_id");
			} else {
				redirect("items/");
			}
		} else {
			$this->load->view("layout/header");
			$this->load->view("items/add", ["item" => $item, "service_id" => $service_id]);
			$this->load->view("layout/footer");
		}
	}
	
	/**
	 * 
	 * @param integer $id The ID of the item to edit
	 */
	public function edit($id) {
		$item = $this->doctrine->em->find("Entity\Item", $id); /* @var $item Entity\Item */
		if (!$item->getChurch()) {
			// No church associated with this item.  Requires a super-admin.
			$this->authorization->RequiresSuperAdmin();
		} else {
			$this->authorization->RequiresPrivilege("items_edit", $item->getChurch()->getId());
		}
		
		$this->load->library("crud");
		
		$this->_set_validation();
		if ($this->form_validation->run()) {
			$this->crud->Bind($item, ["Type", "Title", "Summary", "Length", "Key", "Page", "Details"]);
			$this->doctrine->em->flush();
			
			$this->load->helper("url");
			redirect("/items/");
		} else {
			$this->load->view("layout/header");
			$this->load->view("items/edit", ["item" => $item]);
			$this->load->view("layout/footer");
		}
	}
	
	private function _set_validation() {
		$this->load->library("form_validation");
		$this->form_validation
			->set_rules("Type", "Type", "required|in_list[Congregational,Special,Sermon,Other]")
			->set_rules("Title", "Title", "required|max_length[50]")
			->set_rules("Length", "Length", "max_length[10]|regex_match[/^([0-9]{1,2}(:[0-9]{2})?)?$/]")
			->set_rules("Key", "Key", "max_length[20]")
			->set_rules("Page", "Page", "max_length[10]");
	}
}
