<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Songs extends CI_Controller {
	
	public function index()
	{
		$this->load->library("doctrine");
		$songs = $this->doctrine->em->createQuery("Select s From Entity\Song s Order By s.Title")->execute();
		
		$this->load->view("layout/header");
		$this->load->view("songs/index", ["songs" => $songs]);
		$this->load->view("layout/footer");
	}
	
	public function details_popup($id, $location_id) {
		$this->load->library("doctrine");
		$song = $this->doctrine->em->find("Entity\Song", $id);
		$location = $this->doctrine->em->find("Entity\Location", $location_id);
		$this->load->view("songs/details-popup", ["song" => $song, "location" => $location]);
	}
	
	/**
	 * Delete an event
	 * 
	 * @param integer $id The ID of the event to delete
	 * @param string $confirm "confirm"
	 */
	public function delete($id) {
		
	}
	
	public function add() {
		$this->load->library(["doctrine", "crud"]);
		$this->doctrine->em->getRepository("Entity\Location");
		$song = new \Entity\Song();
		$this->doctrine->em->persist($song);
		
		$this->_setValidation();
		if ($this->form_validation->run()) {
			$this->crud->Bind($song, ["Title", "Details", "Key", "Page", "Length", "Story"]);
			$this->doctrine->em->flush();
			
			$this->load->helper("url");
			redirect("/songs/");
		} else {
			$this->load->view("layout/header");
			$this->load->view("songs/edit", ["song" => $song]);
			$this->load->view("layout/footer");
		}
	}
	
	public function view($id) {
		$this->load->library("doctrine");
		$song = $this->doctrine->em->find("Entity\Song", $id);
		
		$this->load->view("layout/header");
		$this->load->view("songs/view", ["song" => $song]);
		$this->load->view("layout/footer");
	}
	
	public function edit($id, $popup = false) {
		$this->load->library("doctrine");
		$song = $this->doctrine->em->find("Entity\Song", $id); /* @var $event \Entity\Event */
		$this->_setValidation();
		
		if ($this->form_validation->run()) {
			$this->load->library("crud");
			$this->crud->Bind($song, ["Title", "Details", "Key", "Page", "Length", "Story"]);
			$this->doctrine->em->flush();
			
			if (!$popup) {
				$this->load->helper("url");
				redirect("/songs/view/$id/");
			} else {
				$this->output->append_output("Success");
			}
		} else {
			if (!$popup) $this->load->view("layout/header");
			$this->load->view("songs/edit", ["song" => $song]);
			if (!$popup) $this->load->view("layout/footer");
		}
	}
	
	/**
	 * Get a list of all songs in JSON format
	 */
	public function all_json() {
		$this->load->library("doctrine");
		$songs = $this->doctrine->em->createQuery("Select s, es, e, l From Entity\Song s Join s.EventSongs es Join es.Event e Join e.Location l Order By s.Title Asc")->execute();
		
		$songs_json = array();
		foreach ($songs as $song) { /* @var $song \Entity\Song */
			array_push($songs_json, $song->serializeJson());
		}
		
		$this->output->append_output(json_encode($songs_json));
	}
	
	private function _setValidation() {
		$this->load->library("form_validation");
		$this->form_validation->set_rules("Title", "Title", "required|max_length[50]");
	}
}
