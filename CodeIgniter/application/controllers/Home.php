<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->authorization->RequiresLogin();
		$upcoming_services = $this->doctrine->em->createQuery(
			"Select s From Entity\Service s Join s.Church c Where c.Id = ?1 And s.Start > ?0 Order By s.Start Asc"
			)->execute([(new DateTime()), $this->authorization->getCurrentChurchId()]);
		
		$this->load->view("layout/header");
		$this->load->view("home/index", [
			"church"			=> $this->authorization->getCurrentChurch(),
			"upcoming_services"	=> $upcoming_services
		]);
		$this->load->view("layout/footer");
	}
	
	public function test() {
		
	}
}
