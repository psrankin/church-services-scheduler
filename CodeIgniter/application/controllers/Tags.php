<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends CI_Controller {
	
	public function index()
	{
		$this->load->library("doctrine");
		$tags = $this->doctrine->em->createQuery("Select t From Entity\Tag t Order By t.Title Asc")->execute();
		
		$this->load->view("layout/header");
		$this->load->view("tags/index", ["tags" => $tags]);
		$this->load->view("layout/footer");
	}
	
	/**
	 * Get all the valid tags for the currently active church as JSON
	 */
	public function all() {
		$this->authorization->RequiresLogin();
		
		$this->load->library("doctrine");
		$church = $this->authorization->getCurrentChurch();
		$tags = $this->doctrine->em->createQuery("Select t From Entity\Tag t Where t.Church Is Null Or t.Church = ?0")->execute([$church]);
		
		// Loop processing tags, getting their JSON representations
		$tags_json = array();
		foreach ($tags as $tag) { /* @var $tag Entity\Tag */
			array_push($tags_json, $tag->toJsonReady());
		}
		
		$this->output->append_output(json_encode($tags_json));
	}
	
	public function add() {
		$this->load->library(["doctrine", "crud"]);
		$this->doctrine->em->getRepository("Entity\Tag");
		$tag = new \Entity\Event();
		
		$this->_setValidation();
		if ($this->form_validation->run()) {
			$this->crud->Bind($tag, ["Title", "Description"]);
			$this->doctrine->em->flush();
			
			$this->load->helper("url");
			redirect("/tags/");
		} else {
			$this->load->view("layout/header");
			$this->load->view("tags/edit", ["tag" => $tag]);
			$this->load->view("layout/footer");
		}
	}
	
	public function view($id) {
		$this->load->library("doctrine");
		$tag = $this->doctrine->em->find("Entity\Tag", $id);
		
		$this->load->view("layout/header");
		$this->load->view("tags/view", ["tag" => $tag]);
		$this->load->view("layout/footer");
	}
	
	public function edit($id) {
		$this->load->library("doctrine");
		$tag = $this->doctrine->em->find("Entity\Tag", $id); /* @var $event \Entity\Event */
		$this->_setValidation();
		
		if ($this->form_validation->run()) {
			$this->load->library("crud");
			$this->crud->Bind($tag, ["Title", "Details"]);
			$this->doctrine->em->flush();
			
			$this->load->helper("url");
			redirect("/tags/");
		} else {
			$this->load->view("layout/header");
			$this->load->view("tags/edit", ["tag" => $tag]);
			$this->load->view("layout/footer");
		}
	}
	
	private function _setValidation() {
		$this->load->library("form_validation");
		$this->form_validation->set_rules("Title", "Title", "required|max_length[50]");
	}
}
