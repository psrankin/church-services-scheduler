<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Cache-control" content="no-cache">
		<title>Church Scheduler</title>
		
		<link rel="stylesheet" type="text/css" href="/public/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="/public/bootstrap/sb-admin/css/sb-admin-2.css" />
		<link rel="stylesheet" type="text/css" href="/public/css/site.min.css" />
		<link rel="stylesheet" type="text/css" href="/public/jquery-ui/jquery-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="/public/datatables/media/css/dataTables.bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="/public/tinymce/js/tinymce/skins/lightgray/skin.min.css" />
		
		<script type="text/javascript" src="/public/js/jquery-2.1.4.min.js"></script>
	</head>
	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0px;">
				<div class="navbar-header" abp="3">
					<button class="navbar-toggle" type="button" abp="4" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only" abp="5">Toggle navigation</span>
						<span class="icon-bar" abp="6"></span>
						<span class="icon-bar" abp="7"></span>
						<span class="icon-bar" abp="8"></span>
					</button>
					<a class="navbar-brand" href="<?=site_url()?>" abp="9">Church Scheduler</a>
				</div>
				<ul class="nav navbar-top-links navbar-right hidden-xs visible-sm visible-md visible-lg">
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> My Account <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="/my-account/">My Profile</a></li>
						<li><a href="/my-account/change-password/">Change Password</a></li>
						
						<?php if ($this->authorization->HasPrivilege("church_edit")) { ?>
						<li role="separator" class="divider"></li>
						<li><a href="/church/edit/">Church Profile</a></li>
						<?php } ?>
						
						<li role="separator" class="divider"></li>
						<li><a href="/feedback/">Feedback</a></li>
						<li><a href="/my-account/logout">Log Out</a></li>
					  </ul>
					</li>
				</ul>
				<div class="navbar-default sidebar" role="navigation">
					<div class="sidebar-nav navbar-collapse">
						<ul class="nav in" id="side-menu">
							<li><a active-regex="^/$" href="/"><span class="glyphicon glyphicon-home"></span> &nbsp; Home</a></li>
							
							<?php if ($this->authorization->Can("services_edit")) { ?>
							<li><a active-regex="^/(services)|(service-items/)" href="/services/"><span class="glyphicon glyphicon-time"></span> &nbsp; Services</a></li>
							<?php } ?>
							
							<?php if ($this->authorization->Can("items_edit")) { ?>
							<li><a active-regex="^/(items)" href="/items/"><span class="glyphicon glyphicon-music"></span> &nbsp; Items</a></li>
							<?php } ?>
							
							<?php if ($this->authorization->Can("services_edit")) { ?>
							<li><a active-regex="^/locations" href="<?=site_url("locations/")?>"><span class="glyphicon glyphicon-road"></span> Locations</a></li>
							<?php } ?>
							
							<?php if ($this->authorization->Can("users_manage")) { ?>
							<li><a active-regex="^/users" href="<?=site_url("users/")?>"><span class="glyphicon glyphicon-user"></span> People</a></li>
							<?php } ?>
							
							<li class="visible-xs hidden-md"><a active-regex="/my-account/" href="<?=site_url("my-account/")?>">My Account</a></li>
						</ul>
					</div>
				</div>
			</nav>
			<div id="page-wrapper">
				<div class="row">
