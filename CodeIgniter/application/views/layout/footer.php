				</div>
			</div>
		</div>
		<footer>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4" style="text-align:center">
					<a href="http://www.christianprogrammers.com/">Christian Programmers Association</a>
					<div class="Subtle">
						Programming for the glory of our Lord and Saviour Jesus Christ
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4" style="text-align:right">
					<p>
						<a href="<?=site_url('feedback')?>">Feedback</a><br />
						<span class="Subtle">Version: <?=APP_VERSION?></span>
					</p>
				</div>
			</div>
		</footer>
		
		<script type="text/javascript" src="/public/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/public/jquery-ui/jquery-ui.min.js"></script>
		<script type="text/javascript" src="/public/datatables/media/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/public/datatables/media/js/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="/public/bootstrap/sb-admin/js/sb-admin-2.js"></script>
		<script type="text/javascript" src="/public/tinymce/js/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="/public/js/musicscheduler.js"></script>
		<script type="text/javascript" src="/public/js/moment.js"></script>
		<script type="text/javascript">
			$(function() {
				$(".datatables").each(function(i, v) {
					if ($(v).find("tbody tr").length > 10)
						$(v).DataTable();
					else
						 $(v).DataTable({
							"paging":   false,
							// "ordering": false,
							"info":     false
						 });
				});
				
				if ($("textarea.tinymce").length > 0) {
					tinymce.init({
						selector: "textarea.tinymce",
						plugins: [
							"advlist autolink lists link image charmap print preview anchor",
							"searchreplace visualblocks code fullscreen",
							"insertdatetime media table contextmenu paste textcolor"
						],
						toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
						toolbar2: "blockquote forecolor fontselect fontsizeselect",
						menubar: false,
						content_css: "/public/css/tinymce.css"
					});
				}
				
				// $('[data-toggle="popover"]').popover({trigger: 'hover');
				
				  $('[data-toggle="popover"]').popover({container: 'body'});
					// {
					   // trigger: 'hover',
					   // html: true,
					   // placement: 'right',
					   // content: 'hello world'
					// });
				
				$(".moment").each(function(i, v) {
					try { if ($(v).html().trim()) $(v).html(moment($(v).html()).fromNow()).show(); }
					catch (err) {  }
				});
				// alert(moment('2015-01-01 15:00:00').fromNow());
			});
		</script>
	</body>
</html>