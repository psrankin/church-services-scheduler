<!DOCTYPE html>
<html>
	<head>
		<title>Church Scheduler</title>
		
		<link rel="stylesheet" type="text/css" href="/public/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="/public/bootstrap/sb-admin/css/sb-admin-2.css" />
		<link rel="stylesheet" type="text/css" href="/public/css/site.min.css" />
		
		<script type="text/javascript" src="/public/js/jquery-2.1.4.min.js"></script>
	</head>
	<body class="public">
		<div class='container'>
			<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0px;">
				<div class="navbar-header">
					<a class="navbar-brand" href="<?=site_url()?>" abp="9">Church Scheduler</a>
				</div>
			</nav>
			<div class="page-wrapper">