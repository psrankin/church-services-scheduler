<?php /* @var $service Entity\Service */ /* @var $this CI_Controller */ ?>

<h1>Edit Service</h1>

<?=form_open('', ['class' => 'form-default'])?>
	<div class="row">
		<?php $this->crud->QuickLocation("LocationId", "Location", $service->getLocation() ? $service->getLocation()->getId() : null)
			->setResponsiveness("col-xs-12 col-md-6")->Show(); ?>
		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="form-group">
				<label for="StartDate">Date/Time</label>
				<input type="date" class="form-control" value="<?=set_value("StartDate", $service->getStart()->format("m/d/Y"))?>" name="StartDate" id="StartDate" />
				<script type="text/javascript">$(function() { $("#StartDate").datepicker(); });</script>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="form-group">
				<label for="StartTime">Time</label>
				<input type="time" class="form-control" value="<?=set_value("StartTime", $service->getStart()->format("g:ia"))?>" name="StartTime" id="StartTime" />
			</div>
		</div>
		<?php $this->crud->QuickTextarea("Summary", "Summary", $service->getSummary())->Show(); ?>
	</div>
	<hr />
	<div class="row">
		<div class="col-xs-12">
			<input type="submit" value="Save Changes" class="btn btn-primary" />
			<a href="<?=site_url("services/edit/" . $service->getId())?>" class="btn btn-default">Cancel</a>
			<a onclick="return confirm('Are you sure?');" href="<?=site_url("services/delete/" . $service->getId())?>" class="btn btn-danger">Delete</a>
		</div>
	</div>
<?=form_close()?>
