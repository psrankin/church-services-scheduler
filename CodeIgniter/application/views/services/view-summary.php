<?php /* @var $service Entity\Service */ $service_items = $service->getServiceItems(); ?>

<div class="row">
	<div class="col-xs-12" style="max-width:750px;" id="OrderOfEvents">
		<h1><?=$service->getStart()->format("F j, g:ia")?>
			<?php if ($service->getLocation()) { ?>
			<br />
			<small><?=$service->getLocation()->getTitle()?></small>
			<?php } ?>
		</h1>
		
		<?php if ($service->getTotalLength()) { ?>
		<p>Total length: <?=$service->getTotalLength()?></p>
		<?php } ?>
		
		<div id="ServiceItemsWrapper">
			<?php if (count($service->getServiceItems()->toArray()) == 0) { ?>
			<div data-bind="style: {display: ServiceItems().length == 0 ? 'block' : 'none'}" style="text-align:center; display:none;">
				<br /><br />
				<p>Nothing is scheduled for this service yet.</p>
				<br /><br />
			</div>
			<?php } else { ?>
			<div id="ServiceItems">
				<?php foreach ($service_items as $service_item) $this->load->view("service-items/view-summary", ["service_item" => $service_item]); ?>
			</div>
			<?php } ?>
			
			<?php if ($this->authorization->HasPrivilege("services_edit")) { ?>
			<a href="<?=site_url("services/edit/{$service->getId()}")?>" class="btn btn-default"><i class="glyphicon glyphicon-pencil"></i> Edit Service</a>
			<?php } ?>
		</div>
	</div>
</div>