<?php /* @var $this CI_Controller */ ?>
<h1>Upcoming Services</h1>


<?php ob_start(); ?>
<div style="border:1px solid #ddd; border-radius:10px; padding:50px; max-width:700px; text-align:center">
	<p>There are currently no upcoming services scheduled.</p>
	<p>
		<a href="<?=site_url("services/add")?>" class="btn btn-lg btn-primary"><i class="glyphicon glyphicon-time"></i> Schedule New Service</a>
	</p>
</div>
<?php $empty_template = ob_get_clean(); ?>

<?php 
	$this->crud->QuickTable()
		->addField("Time", function($s) { 
			return "<span style='display:none;'>" . $s->getStart()->format("Y-m-d") . "</span>" . $s->getStart()->format("F j, g:ia (l)"); 
		}, false)
		->addField(("Location"), function($s){ return $s->getLocation() ? $s->getLocation()->getTitle() : ""; })
		->addLink("Time", function($s) { return "/services/edit/{$s->getId()}/"; })
		->setEmptyMessage($empty_template)
		->Show($upcoming_services);
?>

<br />
<?php if (count($upcoming_services) > 0) { ?><a href="/services/add/" class="btn btn-primary">Schedule New Service</a> &nbsp;<?php } ?>
<a href="/services/archive/" class="btn btn-default">Past Services</a>
