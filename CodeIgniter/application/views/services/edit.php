<?php /* @var $service Entity\Service */ ?>

<div class="row">
	<div class="col-xs-12 col-md-9 col-lg-6" id="OrderOfEvents">
		<h1><?=$service->getStart()->format("F j, g:ia")?>
			<?php if ($service->getLocation()) { ?>
			<br />
			<small><?=$service->getLocation()->getTitle()?></small>
			<?php } ?>
		</h1>
		<?php if (trim($service->getSummary())) { ?><p class="subtle"><?=$service->getSummary()?></p> <?php } ?>
		
		<?php if ($service->getTotalLength()) { ?>
		<?php if ($service->getTotalLength() !== "0:00") { ?><p>Total length: <?=$service->getTotalLength()?></p><?php } ?>
		<?php } ?>
		
		<br />
		
		<div id="ServiceItemsWrapper">
			<div data-bind="style: {display: ServiceItems().length == 0 ? 'block' : 'none'}" style="text-align:center; display:none;">
				<br /><br />
				<p>Your service is empty.  Click below to schedule a new item.</p>
				<a href="<?=site_url("service-items/add/{$service->getId()}")?>" class="btn btn-lg btn-primary"><i class="glyphicon glyphicon-plus"></i> Add Item</a>
				<br /><br />
			</div>
			<div id="ServiceItems" data-bind="foreach: ServiceItems">
				<div class="ServiceItem" data-bind="attr: {entityid: Id}">
					<div class="Handle"><span class="glyphicon glyphicon-move"></span></div>
					<div class="Length" data-bind="text: Item().LengthNormalized"></div>
					<div class="Content">
						<div class="Title">
							<a data-bind="text: Item().FullTitle, attr: { href: '/service-items/view/' + Id }"></a>
							<a class="EditIcon" data-bind="attr: { href: '/service-items/edit/' + Id }"><i class="glyphicon glyphicon-pencil"></i></a>
							<a class="RemoveIcon" onclick="return confirm('Are you sure?');" data-bind="attr: { href: '/service-items/delete/' + Id }"><i class="glyphicon glyphicon-remove"></i></a>
						</div>
						<div class="Details" data-bind="visible: Item().Summary() != false, text: Item().Summary"></div>
					</div>
				</div>
			</div>
		</div>
		
		<br />
		<a href="/service-items/add/<?=$service->getId()?>/" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i> Add Item</a>
		<a href="/services/edit-master/<?=$service->getId()?>/" class="btn btn-default"><i class="glyphicon glyphicon-pencil"></i> Edit Details</a>
		<a href="<?=site_url("services/share/{$service->getId()}/")?>" class="btn btn-default"><i class="glyphicon glyphicon-user"></i> Share</a>
		<br />
		<br />
	</div>
</div>

<script type="text/javascript" src="/public/js/knockout-3.3.0.js"></script>
<script type="text/javascript" src="/public/js/app/models/Item.js"></script>
<script type="text/javascript" src="/public/js/app/models/ServiceItem.js"></script>
<script type="text/javascript" src="/public/js/app/models/HymnalItem.js"></script>
<script type="text/javascript" src="/public/js/app/models/Tag.js"></script>
<script type="text/javascript" src="/public/js/app/view-models/ServiceItemList.js"></script>
<script type="text/javascript" src="/public/js/app/view-models/ItemList.js"></script>
<script type="text/javascript" src="/public/js/app/dialog.js"></script>

<script type="text/javascript">
	var serviceId = <?=$service->getId()?>;
	var serviceItems = <?=json_encode($service->getServiceItemsJsonReady())?>;
	
	var serviceItemListViewModel	= new ServiceItemListViewModel(serviceId, serviceItems);
	ko.applyBindings(serviceItemListViewModel, $("#ServiceItemsWrapper")[0]);
	
</script>
