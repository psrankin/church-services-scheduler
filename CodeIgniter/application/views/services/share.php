<?php /* @var $ticket_full Entity\LinkTicket */ /* @var $ticket_summary Entity\LinkTicket */ /* @var $service Entity\Service */ ?>

<h1>Share the Service</h1>
<p>Copy the links below and e-mail them to the people with whom you would like to share this service.</p>

<div class='form-small'>

<h2>Summary</h2>
<p>Users with this link can view only the summary of the service, not all the details</p>
<p><input onclick='this.select();' type='text' readonly value='<?=$ticket_summary->getLink()?>' class='form-control' /></p>

<h2>Details</h2>
<p>Users with this link can view all the service details, but cannot edit</p>
<p><input onclick='this.select();' type='text' readonly value='<?=$ticket_full->getLink()?>' class='form-control' /></p>

<a href='<?=site_url("services/edit/{$service->getId()}")?>' class='btn btn-primary'>Back to Service</a>

</div>