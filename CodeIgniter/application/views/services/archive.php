<?php /* @var $this CI_Controller */ ?>

<h1>Archived Services</h1>

<?php
	$this->crud->QuickTable()
		->addField("Time", function($s) { 
			return "<span style='display:none;'>" . $s->getStart()->format("Y-m-d") . "</span>" . $s->getStart()->format("F j, g:ia (l)"); 
		}, false)
		->addField(("Location"), function($s){ return $s->getLocation() ? $s->getLocation()->getTitle() : ""; })
		->addLink("Time", function($s) { return "/services/edit/{$s->getId()}/"; })
		->Show($services);
?>

