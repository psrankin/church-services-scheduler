<?php /* @var $this CI_Controller */ ?>

<div class="row">
	<h1>New Service</h1>
	
	<?=my_validation_errors()?>
	
	<?=form_open('', ['class' => 'form-default'])?>
		<?php $this->crud->QuickLocation("LocationId", "Location")->setResponsiveness("col-xs-12 col-md-6")->Show(); ?>
		<div class="col-xs-12 col-sm-6">
			
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="form-group">
				<label for="StartDate">Date/Time</label>
				<input type="date" class="form-control" name="StartDate" id="StartDate" value="<?=set_value("StartDate")?>" />
				<script type="text/javascript">$(function() { $("#StartDate").datepicker(); });</script>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="form-group">
				<label for="StartTime">Time</label>
				<input type="time" class="form-control" name="StartTime" id="StartTime" value="<?=set_value("StartTime")?>" placeholder="E.g., 11am, 6:30pm" />
			</div>
		</div>
		<?php $this->crud->QuickTextarea("Summary", "Summary")->Show(); ?>
		<div class="col-xs-12">
			<input type="submit" value="Schedule Service" class="btn btn-primary" />
			<a href="<?=  site_url("services")?>" class="btn btn-default">Cancel</a>
		</div>
	<?=form_close()?>
</div>
