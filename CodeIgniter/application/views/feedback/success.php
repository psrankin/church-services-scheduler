<h1>Success</h1>

<p>Thank you for submitting feedback!  We will review your comments/suggestions to
improve the quality of this program.</p>

<a href="<?=$redirect?>" class="btn btn-lg btn-primary">Continue</a>
