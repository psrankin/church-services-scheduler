<?php /* @var $this CI_Controller */ ?>

<h1>Feedback</h1>

<?php 
	$this->crud->QuickTable()
		->addField("URL", function($x) { return $x->getUrl(); })
		->addField("Content", function($x) { return $x->getContent(); })
		->addField("Submitted", function($x) { return $x->getSubmitted()->format("Y-m-d"); })
		->addField("User", function($x) { return $x->getUser()->getEmail(); })
		->addField("Version", function($x) { return $x->getVersion(); })
		->Show($feedback);
		
		
