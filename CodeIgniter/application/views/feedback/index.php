<?php /* @var $this CI_Controller */ ?>

<?=form_open('', ["class" => "form-default"])?>

<div class="col-xs-12">
	<h1>Feedback</h1>

	<p>Thank you for being willing to provide feedback!  This will help us to make our program
	better and easier to use.  Please describe any insights, suggestions, or issues in the 
	box below.</p>
</div>

<?php $this->crud->QuickTextarea("Content", "Content")->setRows(8)->Show(); ?>
<input type="hidden" name="Url" value="<?=set_value("Url", $url)?>" />

<div class="col-xs-12">
	<input type="submit" class="btn btn-primary" value="Send Feedback" /> &nbsp;
	<a href="<?=$url?>" class="btn btn-default">Cancel</a>
</div>
<?=form_close()?>