<?php $this->load->helper("form"); ?>

<h1>Add User</h1>

<?= my_validation_errors(); ?>

<br />

<?=form_open('', ['class' => 'form-default'])?>
	<div class="col-xs-12 col-sm-6">
		<div class="form-group">
			<label for="Title">First Name</label>
			<input type="text" class="form-control" id="FirstName" name="FirstName" placeholder="" value="<?=set_value("FirstName")?>">
		</div>
	</div>
	<div class="col-xs-12 col-sm-6">
		<div class="form-group">
			<label for="Title">Last Name</label>
			<input type="text" class="form-control" id="LastName" name="LastName" placeholder="" value="<?=set_value("LastName")?>">
		</div>
	</div>
	<div class="col-xs-12">
		<div class="form-group">
			<label for="Title">E-mail Address</label>
			<input type="email" class="form-control" id="Email" name="Email" placeholder="" value="<?=set_value("Email")?>">
		</div>
	</div>
	<div class="col-xs-12">
		<div class="form-group">
			<label for="Title">Role</label>
			<select class="form-control" id="RoleId" name="RoleId">
				<?php foreach ($roles as $role) { /* @var $role \Entity\Role */ ?>
				<option value="<?=$role->getId()?>" <?=set_select("Role", $role->getId(), $role->getName() === "Viewer")?>><?=htmlentities($role->getName())?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="col-xs-12">
		<input type="submit" class="btn btn-primary" value="Add New User" />
		<a href="/users/" class="btn btn-default">Cancel</a>
	</div>
<?=form_close()?>
