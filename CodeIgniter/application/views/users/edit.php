<?php /* @var $user Entity\User */ /* @var $this CI_Controller */ ?>

<div class="col-xs-12">
	<h1>Edit Account for <?=$user->getFullName()?></h1>
	<?=my_validation_errors()?>
</div>

<?=form_open('', ['class' => 'form-default'])?>
<?php
	$this->crud->QuickText("FirstName", "First Name", $user->getFirstName())->setResponsiveness("col-xs-12 col-sm-6")->Show();
	$this->crud->QuickText("LastName", "Last Name", $user->getLastName())->setResponsiveness("col-xs-12 col-sm-6")->Show();
	$this->crud->QuickText("Email", "Email", $user->getEmail(), "email")->setResponsiveness("col-xs-12 col-sm-6")->Show();
	
	$roles_array = array();
	foreach ($roles as $role) /* @var $role Entity\Role */ { $roles_array[$role->getId()] = $role->getName(); }
	$this->crud->QuickSelect("RoleId", "Role", $user->getCurrentChurchRole()->getId(), $roles_array)->setResponsiveness("col-xs-12 col-sm-6")->Show();
?>
	
	<div class="col-xs-12">
		<?php $this->crud->QuickCheckbox("ChangePassword", "Change Password")->setValue("ChangePassword")->Show(); ?>
		
		<div id="ChangePasswordPanel" style="display:none;">
			<?php $this->crud->QuickText("Password", "Password", "", "password")->setResponsiveness("col-xs-12 col-sm-6")->Show(); ?>
			<?php $this->crud->QuickText("ConfirmPassword", "Confirm Password", "", "password")->setResponsiveness("col-xs-12 col-sm-6")->Show(); ?>
		</div>
		
		<br /><br />
		<input type="submit" class="btn btn-primary" value="Save Changes" />
		<a href="/users/" class="btn btn-default">Cancel</a>
		<a href="/users/remove/<?=$user->getId()?>/" class="btn btn-danger" onclick="return confirm('Are you sure?')">Remove User</a>
	</div>
<?=form_close()?>

<script type="text/javascript">
	$("#ChangePassword").change(function() {
		var checked = $("#ChangePassword").is(":checked");
		if (checked)
			$("#ChangePasswordPanel").show();
		else
			$("#ChangePasswordPanel").hide().find("input[type=text]").val("");
	});
</script>
