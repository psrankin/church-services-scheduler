<?php /* @var $this CI_Controller */ ?>
<h1>Manage Users</h1>
<p>Control which users have access to the schedule and can make changes</p>

<?php 
	/* @var $quick_table Quick_table */
	$this->crud->QuickTable()
		->addField("Name", function($user) { return $user->getFullName(); })
		->addField("Role", function($user) { return $user->getCurrentChurchRole()->getName(); })
		->addLink("Name", function($user) { return "/users/edit/{$user->getId()}"; })
		->Show($users);
?>

<br />

<a href="/users/add/" class="btn btn-primary">Add New User</a>
