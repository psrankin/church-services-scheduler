<?php /* @var $location \Entity\Location */ ?>
<h1><?=$location->getTitle()?></h1>

<table class="table table-bordered">
	<tr>
		<th>Address</th>
		<td><?=str_replace("\n", "<br />", htmlentities($location->getAddress()))?></td>
	</tr>
	<tr>
		<th>Comments</th>
		<td><?=$location->getComments()?></td>
	</tr>
</table>

<a href="/locations/edit/<?=$location->getId()?>/" class="btn btn-primary">Edit</a>

<hr />

<?php if ($location->getServices()->count() > 0) { ?>
<h2>Services</h2>

<ul>
	<?php foreach ($location->getServices() as $service) { /* @var $service \Entity\Service */ ?>
	<li>
		<a href="<?=site_url("services/view/{$service->getId()}")?>"><?=$service->getStart()->format("F j, Y (l), g:ia")?></a>
	</li>
	<?php } ?>
</ul>
<?php } else { ?>
<p>You have no events listed at this location yet.</p>
<?php } ?>

