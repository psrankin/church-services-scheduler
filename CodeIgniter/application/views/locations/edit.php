<?php /* @var $location \Entity\Location */  ?>
<h1><?php echo $location->getId() ? "Edit" : "New" ?> Location</h1>

<?= validation_errors("<div class='alert alert-warning'>", "</div>"); ?>

<?=form_open('', ['class' => 'form-default'])?>
<div class="form-group">
	<label for="Title">Title</label>
	<input type="text" class="form-control" id="Title" name="Title" placeholder="" value="<?=set_value("Title", $location->getTitle())?>">
</div>
<div class="form-group">
	<label for="Title">Address</label>
	<textarea style="height:75px;" class="form-control" name="Address"><?=set_value("Address", $location->getAddress())?></textarea>
</div>
<div class="form-group">
	<label for="Title">Comments</label>
	<textarea style="height:75px;" class="form-control tinymce" name="Comments"><?=set_value("Comments", $location->getComments(), false)?></textarea>
</div>

<input type="submit" class="btn btn-primary" value="Save" /> &nbsp;
<a href="/locations/" class="btn btn-default">Cancel</a> &nbsp;
<?php if ($location->getId()) { ?>
<a href="/locations/delete/<?=$location->getId()?>/" class="btn btn-danger" onclick="confirm('Are you sure?')">Delete</a>
<?php } ?>
<?=form_close()?>
