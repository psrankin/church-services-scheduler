<h1>Manage Locations</h1>

<table class="datatables table table-striped table-bordered">
	<thead>
		<th>Title</th>
		<th>Address</th>
	</thead>
	<tbody>
		<?php foreach ($locations as $location) { /* @var $location \Entity\Location */ ?>
		<tr>
			<td><a href="/locations/view/<?=$location->getId()?>/"><?=htmlentities($location->getTitle())?></a></td>
			<td><?=htmlentities(str_replace("\n", ", ", $location->getAddress()))?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<a href="/locations/add/" class="btn btn-primary">New Location</a>
