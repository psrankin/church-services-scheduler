<?php /* @var $this CI_Controller */ $this->load->helper("form"); ?>

<h1>Sign Up for a New Account</h1>

<?=my_validation_errors()?>

<?=form_open('', ['class' => 'form-small'])?>
<?php
	$this->crud->QuickText("ChurchTitle", "Church Name")->Show();
	$this->crud->QuickText("Email")->setType("email")->Show();
	$this->crud->QuickText("FirstName", "First Name")->setResponsiveness("col-xs-12 col-sm-6")->Show();
	$this->crud->QuickText("LastName", "Last Name")->setResponsiveness("col-xs-12 col-sm-6")->Show();
	$this->crud->QuickText("Password")->setType("password")->Show();
	$this->crud->QuickText("ConfirmPassword", "Confirm Password")->setType("password")->Show();
?>
	<div class="col-xs-12">
		<input type="submit" class="btn btn-lg btn-primary" value="Create New Account" /> &nbsp;
		<a href="/" class="btn btn-default">Cancel</a>
	</div>
<?=form_close()?>
