<h1>Account Verified</h1>
<p>Thanks for verifying your account!  Now you're ready to log in and start scheduling.</p>
<p>
	<a href='<?=site_url("my-account/login")?>' class='btn btn-lg btn-primary'><i class='glyphicon glyphicon-user'></i> Log In</a>
</p>