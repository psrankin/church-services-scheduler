<h1>Set Password</h1>

<p>Welcome!  Please set an account password to continue.</p>

<?=form_open()?>
<div class="form-group">
	<label for="Password">Password</label>
	<input type="password" class="form-control" id="Password" name="Password" />
</div>
<div class="form-group">
	<label for="Password">Confirm</label>
	<input type="password" class="form-control" id="ConfirmPassword" name="ConfirmPassword" />
</div>

<input type="submit" class="btn btn-primary" value="Set Password" />

<?=form_close()?>
