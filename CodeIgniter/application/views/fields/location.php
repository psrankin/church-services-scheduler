
<select name="<?=$name?>" id="<?=$name?>" class="form-control">
	<option value="-1">(Not specified)</option>
	<?php foreach ($locations as $location) { /* @var $location Entity\Location */ ?>
	<option value="<?=$location->getId()?>" <?=set_select($name, $location->getId(), $location->getId() === $default)?>><?=$location->getTitle()?></option>
	<?php } ?>
</select>
<div style="float:right; margin-top:5px;"><a href="/locations/add/"><i class="glyphicon glyphicon-plus"></i> New Location</a></div>