<h1><?=$item->getId() ? "Edit Item" : "Add Item" ?></h1>

<?=form_open('', ['class' => 'form-default'])?>
	<?php $this->load->view("items/edit-partial", ["item" => $item]); ?>
	
	<div class="col-xs-12">
		<input type="submit" class="btn btn-primary" value="Save Changes" />
		<a href="/items/" class="btn btn-default">Cancel</a>
		<a href="/items/delete/<?=$item->getId()?>" class="btn btn-danger">Delete Item</a>
	</div>
<?=form_close()?>
