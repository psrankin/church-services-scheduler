<?php /* @var $item Entity\Item */ /* @var $this CI_Controller */ ?>
<h1>Add New Item</h1>
<?=my_validation_errors()?>
<?=form_open('', ['class' => 'form-default'])?>
<?php $this->load->view("items/edit-partial", ["item" => $item]); ?>

<div class="col-xs-12">
	<input type="submit" class="btn btn-primary" value="Add Item" />

	<?php if ($service_id) { ?>
	<a href="/services/edit/<?=$service_id?>" class="btn btn-default">Cancel</a>
	<?php } else { ?>
	<a href="/items/" class="btn btn-default">Cancel</a>
	<?php } ?>
</div>
<?=form_close()?>
