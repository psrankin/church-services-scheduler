<?php /* @var $item Entity\Item */ /* @var $this CI_Controller */ ?>

<h1><?=$item->getTitle()?></h1>

<?php $this->load->view("items/view-partial", ["item" => $item]); ?>
