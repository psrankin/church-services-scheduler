<?php 
	/* @var $item Entity\Item */
	/* @var $this CI_Controller */ 
	
	my_validation_errors();
	
	?><div class="row"><?php
	$this->crud->QuickSelect("Type", "Type", $item->getType())
		->setOptionsFlat(["Congregational", "Special", "Sermon", "Other"])
		->setResponsiveness("col-xs-6 col-sm-3 col-md-3 col-lg-3")->Show();
	
	$this->crud->QuickText("Title", "Title", $item->getTitle())
		->setResponsiveness("col-xs-6 col-lg-5")
		->setTooltip("This could be the title of the sermon, the name of the song, etc.")
		->Show();
	
	?></div><div class="row"><?php
	
	$this->crud->QuickText("Key", "Key", $item->getKey())
		->setResponsiveness("col-xs-6 col-sm-3 col-md-3 col-lg-2")
		->setTooltip("If you don't know how to fill this in, that's okay--leave it blank.  In the future we'll provide an easy-to-use tool to help you fill in the musical key.")
		->Show();
	
	$this->crud->QuickText("Length", "Length", $item->getLength())
		->setResponsiveness("col-xs-6 col-sm-3 col-md-3 col-lg-2")
		->setTooltip("The length of the song or item in minutes.  For instance, 40 could indicate that the sermon is 40 minutes long, or 3:20 means the song is three minutes and twenty seconds long.")
		->setPattern("[0-9]+(:[0-5]{1}[0-9]{1})?")->setTitle("30, 3:25, 0:30, 45, etc.")
		->Show();
	
	$this->crud->QuickText("Page", "Page", $item->getPage())
		->setResponsiveness("col-xs-6 col-sm-3 col-md-3 col-lg-2")
		->setTooltip("This is used mostly for congregational hymns.  It's the number of the song in the hymnbook.")
		->Show();
	
	?></div><div class="row"><?php
	$this->crud->QuickTextarea("Summary", "Summary", $item->getSummary())
		->setTooltip("A brief summary of this item.  For instance, a song might have this: 'Organ + Piano + Choir, 3 verses'.")
		->Show();
	
	$this->crud->QuickTextarea("Details", "Details", $item->getDetails())->enableTinyMce()
		->setTooltip("Place any detailed documentation here.  For instance, you can put a roadmap for a song, the main points in the sermon, or any other comments to communicate to behind-the-scenes workers.  Or, you can jsut leave this blank.")
		->Show();
	?></div>