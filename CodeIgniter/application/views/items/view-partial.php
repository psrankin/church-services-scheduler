<?php /* @var $item Entity\Item */ /* @var $this CI_Controller */ /* @var $service_item Entity\ServiceItem */
	$item = $service_item->getItem();
	
	$performances = $this->doctrine->em->createQuery(
		"Select s From Entity\Service s Where Exists (Select si From Entity\ServiceItem si Join si.Item i Join si.Service s2 Where i.Id = ?0 And s = s2) And s.Id <> ?1 " .
		"Order By s.Start Desc")->execute([$item->getId(), $service_item->getService()->getId()]);
	
	// When viewing service items, display the last time the item was performed at the specified location
	$last_performed_here = null;
	if ($service_item && $service_item->getService()->getLocation()) {
		// Find th e last time this item was performed for a service at this same location, if ever
		$last_performances = $this->doctrine->em->createQuery(
			"Select si, s From Entity\ServiceItem si Join si.Service s Join si.Item i Join s.Location l Where s.Start < ?0 And i.Id = ?1 And l.Id = ?2 Order By s.Start Desc")
			->setMaxResults(1)->execute([
				$service_item->getService()->getStart(),
				$service_item->getItem()->getId(),
				$service_item->getService()->getLocation()->getId()
			]);
		if (count($last_performances) > 0) {
			$last_performance = $last_performances[0]; /* @var $last_performance Entity\ServiceItem */
			$last_performed_here = $last_performance->getService()->getStart();
		}
	}
?>

<h1><?=$item->getTitle()?></h1>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#General" role="tab" data-toggle="tab">General Info</a></li>
    <li role="presentation"><a href="#PastPerformances" role="tab" data-toggle="tab">Past Performances</a></li>
</ul>
<br />
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="General">
		<?php
			$this->crud->QuickViewTable()
				->addRow("Title",	$item->getTitle())
				->addRow("Type",	$item->getType())
				->addRow("Key",		$item->getKey())
				->addRow("Length",	$item->getLength())
				->addRow("Page",	$item->getPage())
				->addRow("Summary", $item->getSummary())
				->addRow("Details", $item->getDetails())
				->Show();
		?>
	</div>
	
	<div role="tabpanel" class="tab-pane" id="PastPerformances">
		<?php if ($last_performed_here || $service_item->getService()->getLocation()) { ?>
		<p>Last performed at <?=$service_item->getService()->getLocation()->getTitle()?>: <?=$last_performed_here ? $last_performed_here->format("F j, Y") : "Never"?></p>
		<?php } ?>
		
		<?php
			$this->crud->QuickTable()
				->addField("Date", function($x) { return "<span style='display:none;'>" . $x->getStart()->format("Y-m-d") . "</span>" . $x->getStart()->format("M j, Y"); }, false)
				->addLink("Date", function($x) { return site_url("services/view/{$x->getId()}"); })
				->addField("Location", function($x) { return $x->getLocation() ? $x->getLocation()->getTitle() : ""; })
				->addLink("Location", function ($x) { return $x->getLocation() ? site_url("locations/view/{$x->getLocation()->getId()}") : null; })
				->setEmptyMessage("This item has not been scheduled for any services.")
				->disableDatatables()
				->Show($performances);
		?>
	</div>
</div>
  