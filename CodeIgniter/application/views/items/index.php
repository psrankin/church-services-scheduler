<?php /* @var $this CI_Controller */ ?>
<h1>Manage Items</h1>

<?php 
	$this->crud->QuickTable()
		->addField("Title", function($x) { return $x->getTitleFullSortable(); })
		->addLink("Title", function($x) { return "/items/edit/{$x->getId()}"; })
		->addField("Type", function($x) { return $x->getType(); })
		->Show($items);
?>