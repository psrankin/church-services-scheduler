<?php /* @var $service_item Entity\ServiceItem */ /* @var $item Entity\Item */ $item = $service_item->getItem(); ?>

<div class="ServiceItem View Detailed <?=$item->getType()?>">
	<div class="Length"><?=html_escape($item->getLength())?></div>
	<div class="Content">
		<h3><?=$item->getTitleFull()?></h3>
		
		<?php if ($item->getSummary()) { ?>
		<div class="Summary"><?=html_escape($item->getSummary())?></div>
		<?php } ?>
		
		<?php if ($item->getDetails()) { ?>
		<div class="Details"><?=html_escape($item->getDetails())?></div>
		<?php } ?>
	</div>
</div>
