<?php /* @var $service_item Entity\ServiceItem */ ?>

<h1>Edit Service Item</h1>
<p><a href="<?=site_url("services/edit/{$service_item->getService()->getId()}")?>">Back to Service</a></p>

<?=form_open('', ['class' => 'form-default'])?>
	<?php $this->load->view("items/edit-partial", ["item" => $service_item->getItem()]); ?>
	
	<div class="col-xs-12">
		<br />
		<input type="submit" class="btn btn-primary" value="Save Changes" />
		<a class="btn btn-default" href="<?=site_url("services/edit/{$service_item->getService()->getId()}")?>">Cancel</a>
		<a class="btn btn-default" href="<?=site_url('service-items/move-up/' . $service_item->getId())?>"><i class="glyphicon glyphicon-arrow-up"></i> Move Up</a>
		<a class="btn btn-default" href="<?=site_url('service-items/move-down/' . $service_item->getId())?>"><i class="glyphicon glyphicon-arrow-down"></i> Move Down</a>
		<a class="btn btn-danger" href="<?=site_url('service-items/delete/' . $service_item->getId())?>"><i class="glyphicon glyphicon-minus"></i> Remove from Service</a>
	</div>
<?=form_close()?>
