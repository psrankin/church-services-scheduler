<?php /* @var $service_item Entity\ServiceItem */ /* @var $item Entity\Item */ $item = $service_item->getItem(); ?>

<div class="ServiceItem View Summary <?=$item->getType()?>">
	<div class="Length"><?=html_escape($item->getLength())?></div>
	<div class="Content">
		<p><strong><?=$item->getTitleFull()?></strong>
			<?php if ($item->getSummary()) { ?>
			-- <?=html_escape($item->getSummary())?>
			<?php } ?>
		</p>
	</div>
</div>
