<?php 
	/* @var $service Entity\Service */
	/* @var $this CI_Controller */ 
	$location_id = $service->getLocation() ? $service->getLocation()->getId() : null;
?>
<?php $this->load->helper("form"); ?>
<h1>Add Item to Service
	<br /><small><?=$service->getStart()->format("F j, g:ia")?><?php if ($service->getLocation()) { ?> at <?=$service->getLocation()->getTitle()?><?php } ?></small></h1>

<?=my_validation_errors()?>

<?=form_open("items/add/{$service->getId()}", ['class' => 'form-default'])?>
	<div class="form-group-lg">
		<label for="Title">Title</label>
		<div class="input-group">
			<div class="input-group-addon"><i class="glyphicon glyphicon-search"></i></div>
			<input type="text" class="form-control" id="Title" name="Title" placeholder="Enter title or search">
		</div>
	</div>
	<br />
	<div style="float:right">
		<a class="btn btn-default" href="/services/edit/<?=$service->getId()?>/">Cancel</a>
		<input type="submit" class="btn btn-primary" value="Brand New Item &raquo;" />
	</div>
	
	<h2>Suggestions</h2>
	<table id="Suggestions" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Type</th>
				<th>Details</th>
				<th>Last Performed Here</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($items as $item) { /* @var $item Entity\Item */ ?>
			<tr>
				<td><a href="<?=site_url("service-items/add-existing/{$service->getId()}/{$item->getId()}")?>"><?=html_escape($item->getTitle())?></a></td>
				<td><?=html_escape($item->getType())?></td>
				<td><?=html_escape($item->getDetails())?></td>
				<td>
					<div class="moment">
						<?php
						$last_performed = $item->getLastPerformedAt($location_id);
						if ($last_performed) echo $last_performed->format("Y-m-d");
						?>
					</div>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
<?=form_close()?>

<script type="text/javascript">
	$(function() {
		var dtable;
		if ($("table#Suggestions tbody tr").length > 10)
			var dtable = $("table#Suggestions").DataTable({search:false});
		else
			var dtable = $("table#Suggestions").DataTable({
				paging:		false,
				info:		false
			});
		
		$('#Title').keyup(function(){
			dtable.search($(this).val()).draw() ;
		});
	});
</script>

<style type="text/css">
	.dataTables_filter, .dataTables_info { display: none; }
</style>
