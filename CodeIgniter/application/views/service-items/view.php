<?php 
	/* @var $service_item Entity\ServiceItem */
	/* @var $this CI_Controller */ 
	$item = $service_item->getItem(); /* @var $item Entity\Item */
?>
<br />
<p><a href="<?=site_url("services/edit/{$service_item->getService()->getId()}")?>" class="btn btn-default">Back to Service</a></p>

<?php $this->load->view("items/view-partial", ["item" => $service_item->getItem()]); ?>

<a href="<?=site_url("service-items/edit/" . $service_item->getId())?>" class="btn btn-primary">Edit Details</a>
<a class="btn btn-default" href="<?=site_url("services/edit/{$service_item->getService()->getId()}")?>">Back to Service</a>
<a class="btn btn-default" href="<?=site_url('service-items/move-up/' . $service_item->getId())?>"><i class="glyphicon glyphicon-arrow-up"></i> Move Up</a>
<a class="btn btn-default" href="<?=site_url('service-items/move-down/' . $service_item->getId())?>"><i class="glyphicon glyphicon-arrow-down"></i> Move Down</a>
<a class="btn btn-danger" href="<?=site_url('service-items/delete/' . $service_item->getId())?>"><i class="glyphicon glyphicon-minus"></i> Remove from Service</a>
