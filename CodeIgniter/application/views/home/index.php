<?php /* @var $church Entity\Church */ $can_edit = $this->authorization->Can("services_edit"); ?>
<h1><?=$church->getTitle()?></h1>

<br />

<div class="IconMenuItem">
	<div class="Icon"><i class="glyphicon glyphicon-time"></i></div>
	<?php if (count($upcoming_services) > 0) { ?>
	<div class="Content">
		<h3>Upcoming Services</h3>
		<ul class="Links">
			<?php foreach ($upcoming_services as $service) { /* @var $service Entity\Service */ ?>
			<li><a href="/services/<?=$can_edit ? "edit" : "view"?>/<?=$service->getId()?>"><?=$service->getStart()->format("M j")?></a></li>
			<?php } ?>
		</ul>
	</div>
	
	<?php if ($can_edit) { ?>
	<div style="float:right;"><a class="btn" href="/services/add/"><i class="glyphicon glyphicon-plus"></i> New Service</a></div>
	<?php } ?>
	
	<div style="clear:both;"></div>
	
	<?php } else if ($can_edit) { ?>
	<div class="Content">
		<br />
		<p>You currently have no services scheduled.</p>
		<p><a class="btn btn-lg btn-primary" href="<?=site_url("services/add")?>"><i class="glyphicon glyphicon-time"></i> Schedule a Service</a></p>
	</div>
	<?php } else { ?>
	<div class="Content">
		<br />
		<p>You currently have no services scheduled.</p>
	</div>
	<?php } ?>
</div>


