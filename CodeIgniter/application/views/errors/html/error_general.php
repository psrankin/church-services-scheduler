<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'views/layout/isolated/header.php';
?>
	<h1><?php echo $heading; ?></h1>
	<p><?php echo $message; ?></p>
	<hr />
	<p><a href='/' class="btn btn-lg btn-primary"><i class="glyphicon glyphicon-home"></i> Back Home</a></p>
<?php require_once APPPATH.'views/layout/isolated/footer.php'; ?>