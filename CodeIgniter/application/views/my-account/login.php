<?php $this->load->helper("form"); ?>
<h1>Login</h1>

<?=my_validation_errors()?>

<?=form_open('', ['class' => 'form-small'])?>
<div class="form-group">
	<label for="Title">Username or E-mail</label>
	<div class="input-group">
		<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
		<input type="text" class="form-control" id="Username" name="Username" placeholder="someone@example.com" value="<?=set_value("Username")?>" />
	</div>
</div>
<div class="form-group">
	<label for="Title">Password</label>
	<div class="input-group">
		<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
		<input type="password" class="form-control" id="Password" name="Password" placeholder="" value="<?=set_value("Password")?>" />
	</div>
</div>

<input type="submit" value="Login" class="btn btn-primary" /> &nbsp;
<a href="<?=site_url("my-account/reset-password/")?>">Forgot Password</a>
<br /><br />
<a href="<?=site_url("signup")?>" class="btn btn-default">Create New Church Account</a>
<?=form_close()?>