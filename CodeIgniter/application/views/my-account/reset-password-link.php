<h1>Reset Password</h1>

<?=form_open('', ['class' => 'form-small'])?>
	<div class="form-group-lg">
		<label for="Password">Please enter your new password</label>
		<div class="input-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
			<input type="password" class="form-control" id="Password" name="Password" placeholder="" value="<?=set_value("Password")?>" />
		</div>
	</div>
	<div class="form-group-lg">
		<label for="PasswordRepeat">Repeat password</label>
		<div class="input-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
			<input type="password" class="form-control" id="ConfirmPassword" name="ConfirmPassword" placeholder="" value="<?=set_value("ConfirmPassword")?>" />
		</div>
	</div>
	<br />
	<input type="submit" class="btn btn-lg btn-primary" value="Change Password" />
<?=form_close()?>