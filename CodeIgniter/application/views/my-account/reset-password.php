<h1>Reset Password</h1>

<?=form_open('', ['class' => 'form-small'])?>
	<div class="form-group-lg">
		<label for="Email">Please enter your e-mail address</label>
		<div class="input-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
			<input type="email" class="form-control" id="Password" name="Email" placeholder="" value="<?=set_value("Email")?>" />
		</div>
	</div>
	
	<br />
	
	<input type="submit" class="btn btn-lg btn-primary" value="Reset Password" />
	<a href="<?=site_url("my-account/login")?>"
<?=form_close()?>