<?php /* @var $this CI_Controller */ /* @var $user Entity\User */ ?>
<h1>My Account</h1>

<?=my_validation_errors()?>

<?=form_open('', ['class' => 'form-small'])?>
<?php
	$this->crud->QuickText("FirstName", "First Name", $user->getFirstName())->Show();
	$this->crud->QuickText("LastName", "Last Name", $user->getLastName())->Show();
?>
<div class="col-xs-12">
	<input type="submit" class="btn btn-primary" value="Save Changes" /> &nbsp;
	<a href="<?=site_url()?>" class="btn btn-default">Cancel</a>
</div>
<?=form_close()?>
