<?php /* @var $this CI_Controller */ ?>
<h1>Change Password</h1>

<?=my_validation_errors()?>

<?=form_open('', ['class' => 'form-small'])?>
<?php 
	$this->crud->QuickText("Password", "New Password")->setType("password")->Show();
	$this->crud->QuickText("ConfirmPassword", "Confirm Password")->setType("password")->Show();
?>
	<div class="col-xs-12">
		<input type="submit" class="btn btn-primary" value="Change Password" /> &nbsp;
		<a href="/my-account/" class="btn btn-default">Cancel</a>
	</div>
<?=form_close()?>

