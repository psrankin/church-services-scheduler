<?php /* @var $this CI_Controller */ /* @var $church Entity\Church */ ?>
<h1>Edit Church Profile</h1>

<?=form_open('', ['class' => 'form-default'])?>
<?php
$this->crud->QuickText("Title", "Church Name", $church->getTitle())->Show();
$this->crud->QuickTextarea("Description", "Description/Notes", $church->getDescription())->enableTinyMce()->Show();
?>

<div class="col-xs-12">
	<input type="submit" class="btn btn-primary" value="Save Changes" />
	<a href="<?=site_url()?>" class="btn btn-default">Cancel</a>
</div>
<?=form_close()?>