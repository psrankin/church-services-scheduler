<?php

require_once('Quick_field.php');

class Quick_select extends Quick_field {
	private $options = array();
	public function setOptions($options) { $this->options = $options; return $this; }
	public function setOptionsFlat($options) { foreach ($options as $option) { $this->options[$option] = $option; } return $this; }
	
	protected function getFieldHtml() {
		ob_start();
		?><select id="<?=$this->name?>" name="<?=$this->name?>" class="form-control">
			<?php foreach ($this->options as $value => $text) { ?>
			<option value="<?=$value?>" <?=set_select($this->name, $value, $this->default == $value)?>><?=htmlentities($text)?></option>
			<?php } ?>
		</select>
			
			<?php
		return ob_get_clean();
	}
}