<?php

require_once('Quick_field.php');

class Quick_textarea extends Quick_field {
	private $is_tiny_mce = false;
	public function enableTinyMce() { $this->is_tiny_mce = true; if (!$this->rows) $this->rows = 15; return $this; }
	
	private $rows = null;
	public function setRows($rows) { $this->rows = $rows; return $this; }
	
	public function getFieldHtml() {
		ob_start();
		?><textarea rows="<?=$this->rows ? $this->rows : 4?>" class="form-control<?=$this->is_tiny_mce ? " tinymce" : ""?>" id="<?=$this->name?>" name="<?=$this->name?>"><?=set_value($this->name, $this->default, false)?></textarea><?php
		return ob_get_clean();
	}
}

