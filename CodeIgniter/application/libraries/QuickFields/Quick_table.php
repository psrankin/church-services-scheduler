<?php

class Quick_table {
	/**
	 * An array of field headers to callbacks
	 */
	private $fields = array();
	
	private $disable_datatables = false;
	public function disableDatatables() { $this->disable_datatables = true; return $this; }
	
	private $empty_message = "There are currently no items to view.";
	public function setEmptyMessage($message) { $this->empty_message = $message; return $this; }
	
	public function addField($name, $callback, $escape = true) {
		$this->fields[$name] = ["value" => $callback, "escape" => $escape];
		return $this;
	}
	
	public function addLink($name, $callback) {
		$this->fields[$name]["link"] = $callback;
		return $this;
	}
	
	public function __construct() {
		
	}
	
	public function GetHtml($entities) {
		if (count($entities) === 0) { return $this->empty_message; }
		
		$opener = "<table class='table table-bordered table-striped " . ($this->disable_datatables ? '' : 'datatables') . "'>";
		$closer = "</table>";
		
		$thead = "<thead><tr>";
		foreach ($this->fields as $name => $callback) { $thead .= "<th>$name</th>"; }
		$thead .= "</tr></thead>";
		
		// Loop processing each entity, creating the record for it
		$tbody = "<tbody>";
		foreach ($entities as $entity) {
			$entity_html = "<tr>";
			
			foreach ($this->fields as $name => $options) {
				$value_callback = $options["value"];
				$value = $value_callback($entity);
				$link = null;
				if (isset($options["link"])) {
					$link_callback = $options["link"];
					$link = $link_callback($entity);
				}
				
				if ($options["escape"])
					$value_html = htmlentities($value);
				else
					$value_html = $value;
				
				if ($link)
					$value_html = "<a href='$link'>" . $value_html . "</a>";
				
				$entity_html .= "<td>" . $value_html . "</td>";
			}
			
			$tbody .= $entity_html;
		}
		$tbody .= "</tbody>";
		
		return $opener . $thead . $tbody . $closer;
	}
	
	public function Show($entities) {
		echo $this->GetHtml($entities);
	}
}