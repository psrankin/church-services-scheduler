<?php

class Quick_view_table {
	/**
	 *
	 * @var array An associative array of rows to display, with the following keys:
	 *   Title, Value, DisplayIfEmpty, EscapeHtml
	 */
	private $rows = array();
	
	/**
	 * @var boolean Whether to display empty rows by default
	 */
	private $display_if_empty = false;
	
	public function displayEmptyRows() { $this->display_if_empty = true; return $this; }
	
	public function addRow($title, $value, $display_if_empty = null, $escape_html = true) {
		array_push($this->rows, [
			"Title"				=> $title,
			"Value"				=> $value,
			"DisplayIfEmpty"	=> $display_if_empty,
			"EscapeHtml"		=> $escape_html
		]);
		
		return $this;
	}
	
	public function GetHtml()
	{
		$self = $this;
		$filtered_rows = array_filter($this->rows, function($r) use ($self) {
			if ($r['Value']) return true;
			if ($r['DisplayIfEmpty']) return true;
			if ($self->display_if_empty && $r['DisplayIfEmpty'] !== false) return true;
			return false;
		});
		if (count($filtered_rows) === 0) return "No data to display.";
		
		ob_start();
		?><table class="table table-bordered table-striped">
			<?php foreach ($filtered_rows as $row) { ?>
			<tr>
				<th><?=htmlentities($row['Title'])?></th>
				<td><?=$row['EscapeHtml'] ? htmlentities($row['Value']) : $row['Value']?></td>
			</tr>
			<?php } ?>
		</table><?php
		
		return ob_get_clean();
	}
	
	public function Show() {
		echo $this->GetHtml();
	}
}
