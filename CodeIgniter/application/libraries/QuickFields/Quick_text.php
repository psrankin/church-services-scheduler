<?php

require_once('Quick_field.php');

class Quick_text extends Quick_field {
	private $type = "text";
	public function setType($type) { $this->type = $type; return $this; }
	
	private $placeholder;
	public function setPlaceholder($placeholder) { $this->placeholder = $placeholder; return $this; }
	
	private $pattern;
	public function setPattern($pattern) { $this->pattern = $pattern; return $this; }
	
	private $title = null;
	public function setTitle($title) { $this->title = $title; return $this; }
	
	public function getFieldHtml() {
		ob_start();
		?><input type="<?=$this->type?>" class="form-control" <?=$this->title ? 'title="' . $this->title . '"' : ""?> id="<?=$this->name?>" <?=$this->pattern ? "pattern=\"" . str_replace('"', '\\"', $this->pattern) . "\"" : ""?> name="<?=$this->name?>" placeholder="" value="<?=set_value($this->name, $this->default)?>"><?php
		return ob_get_clean();
	}
}

