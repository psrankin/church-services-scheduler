<?php

require_once('Quick_field.php');

class Quick_checkbox extends Quick_field {
	protected $value;
	public function setValue($value) { $this->value = $value; return $this; }
	
	public function __construct() {
		$this->include_header_footer = false;
	}
	
	public function getFieldHtml() {
		ob_start();
		
		?><div class="<?=$this->responsiveness?>">
			<div class="checkbox">
				<label>
					<input type="checkbox" name="<?=$this->name?>" id="<?=$this->name?>" <?=set_checkbox($this->name, $this->value, $this->default)?>> <?=  htmlentities($this->getLabel())?>
				</label>
			</div>
		</div><?php
		
		return ob_get_clean();
	}
}

