<?php

abstract class Quick_field {
	protected $include_header_footer = true;
	
	protected $name;
	public function setName($name, $label = null) { $this->name = $name; if ($label) $this->label = $label; return $this; }
	
	protected $label;
	public function setLabel($label) { $this->label = $label; return $this; }
	protected function getLabel() { return $this->label ? $this->label : $this->name; }
	
	protected $default = '';
	public function setDefault($default) { $this->default = $default; return $this; }
	
	protected $responsiveness = 'col-xs-12';
	public function setResponsiveness($responsiveness) { $this->responsiveness = $responsiveness; return $this; }
	
	protected $tooltip = null;
	protected $tooltip_title = null;
	public function setTooltip($tooltip, $title = 'Quick Tip') { $this->tooltip = $tooltip; $this->tooltip_title = $title; return $this; }
	
	protected abstract function getFieldHtml();
	
	protected function getHeader() {
		ob_start();
		
		?><div class="<?=$this->responsiveness?>">
			<div class="form-group">
				<label for="<?=$this->name?>"><?=$this->getLabel()?>
				<?php if ($this->tooltip) {
					?><a tabindex="-1" role="button" class="FieldHelpButton" data-toggle="popover" data-trigger="focus" title="<?=html_escape($this->tooltip_title)?>" data-content="<?=html_escape($this->tooltip)?>">
						<i class="glyphicon glyphicon-question-sign"></i>
					</a><?php
				} ?>
				</label><?php
		
		return ob_get_clean();
	}
	
	protected function getFooter() {
		ob_start();
		
		?></div>
			</div><?php
		
		return ob_get_clean();
	}
	
	public function Show() {
		if ($this->include_header_footer) echo $this->getHeader();
		echo $this->getFieldHtml();
		if ($this->include_header_footer) echo $this->getFooter();
	}
}
