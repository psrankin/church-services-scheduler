<?php

require_once('Quick_location.php');

class Quick_location extends Quick_field {
	protected function getFieldHtml() {
		// First, get a list of locations for this particular church
		$ci =& get_instance(); /* @var $ci CI_Controller */
		$locations = $ci->doctrine->em->getRepository("Entity\Location")->findBy(["Church" => $ci->authorization->getCurrentChurchPartial()]);
		
		return $ci->load->view("fields/location", [
			"name"				=> $this->name,
			"label"				=> $this->label,
			"default"			=> $this->default,
			"locations"			=> $locations
		], true);
	}
}