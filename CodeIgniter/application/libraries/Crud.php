<?php

require_once('QuickFields/Quick_table.php');
require_once('QuickFields/Quick_text.php');
require_once('QuickFields/Quick_select.php');
require_once('QuickFields/Quick_checkbox.php');
require_once('QuickFields/Quick_textarea.php');
require_once('QuickFields/Quick_view_table.php');
require_once('QuickFields/Quick_location.php');

/**
 * Manage basic operations for creating, reading, updating, and deleting records
 */
class Crud {
	/**************************************************************************/
	/*                          P R O P E R T I E S                           */
	/**************************************************************************/
	
	/**
	 * @var CI_Controller Access to CodeIgniter
	 */
	private $ci;
	
	/**
	 * @var object The model being edited/viewed
	 */
	private $model;
	public function Model($model) { $this->model = $model; return $this; }
	
	/**
	 * A list of links to display 
	 * @var type 
	 */
	private $links = array(
	);
	
	/**************************************************************************/
	/*                             M E T H O D S                              */
	/**************************************************************************/
	/**
	 * Constructor, construct a CRUD object
	 */
	public function __construct() {
		$this->ci =& get_instance();
		$this->ci->load->helper("form");
	}
	
	public function QuickTable() { return new Quick_table(); }
	
	public function QuickViewTable() { return new Quick_view_table(); }
	
	/**
	 * 
	 * @param type $name
	 * @param type $label
	 * @param type $default
	 * @param type $type
	 * @return \Quick_text
	 */
	public function QuickText($name = null, $label = null, $default = null, $type = null) { 
		$quick_text = new Quick_text();
		if ($name)		$quick_text->setName($name);
		if ($label)		$quick_text->setLabel ($label);
		if ($default)	$quick_text->setDefault($default);
		if ($type)		$quick_text->setType($type);
		
		return $quick_text;
	}
	
	/**
	 * @return \Quick_location
	 */
	public function QuickLocation($name = null, $label = null, $default = null) {
		$quick_location = new Quick_location();
		
		if ($name)		$quick_location->setName($name);
		if ($label)		$quick_location->setLabel($label);
		if ($default)	$quick_location->setDefault($default);
		
		return $quick_location;
	}
	
	/**
	 * @return \Quick_select
	 */
	public function QuickSelect($name = null, $label = null, $default = null, $options = null) {
		$quick_select = new Quick_select();
		
		if ($name)		$quick_select->setName($name);
		if ($label)		$quick_select->setLabel($label);
		if ($default)	$quick_select->setDefault($default);
		if ($options)	$quick_select->setOptions($options);
		
		return $quick_select;
	}
	
	public function QuickTextarea($name = null, $label = null, $default = null) {
		$quick_textarea = new Quick_textarea();
		
		if ($name)		$quick_textarea->setName($name);
		if ($label)		$quick_textarea->setLabel($label);
		if ($default)	$quick_textarea->setDefault($default);
		
		return $quick_textarea;
	}
	
	public function QuickCheckbox($name = null, $label = null, $value = null, $default = null) {
		$quick_checkbox = new Quick_checkbox();
		
		if ($name) $quick_checkbox->setName($name);
		if ($label) $quick_checkbox->setLabel($label);
		if ($value) $quick_checkbox->setValue($value);
		if ($default) $quick_checkbox->setDefault($default);
		
		return $quick_checkbox;
	}
	
	/**
	 * Bind input POST data to a specified model
	 * 
	 * @param object $model The model to which to bind input data
	 * @param array $fields An array of fields to bind to input data
	 */
	public function Bind($model, $fields) {
		$this->ci->load->helper("security");
		
		// Loop processing each  bine field, binding the input data to its value
		foreach ($fields as $field) {
			$field = clean_identifier($field);
			$model->{"set" . $field}($_POST[$field]);
		}
	}
	
	/**
	 * Return the viewing HTML for this model
	 * 
	 * @param object $model The model to view
	 */
	public function Read($model) {
		// TODO: Implement
	}
	
	/**
	 * Return the "edit" portion of the HTML for this model
	 * 
	 * @param type $model
	 */
	public function Update($model) {
		// TODO: Implement
	}
	
	/**
	 * Display the HTML allowing the user to update one or more fields
	 * 
	 * @param object $model the model whose fields to update
	 * @param type $fields
	 */
	public function UpdateFields($model, $fields) {
		// TODO: Implement
	}
	
	/**
	 * A list of the models to display to the user for managing/viewing
	 */
	public function Listing($models) {
		// TODO: Implement
	}
}
