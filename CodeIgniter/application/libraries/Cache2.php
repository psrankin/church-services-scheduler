<?php

class Cache2 {
	/**
	 *
	 * @var CI_Controller $ci
	 */
	private $ci;
	
	const DEFAULT_TTL = 300;
	const CACHING_ON = true;
	
	function __construct() {
		$this->ci =& get_instance();
		$this->ci->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}
	
	/**
	 * Check a cached value, returning the value if it exists, or FALSE otherwise
	 * 
	 * @param string $id The cache ID
	 * @param array $locations The array of locations to search
	 */
	public function TryGet($id, $locations = null) {
		// TODO: Implement
	}
	
	public function get($id, $callback, $user_specific = true, $ttl = self::DEFAULT_TTL) {
		if (!self::CACHING_ON) return $callback();
		$id = $this->getFullId($id, $user_specific);
		
		$value = null;
		
		// First, check the CI cache
		if ($value = $this->ci->cache->get($id))
			return $value;
		
		// Perform the user-specified callback and return the results
		$value = $callback();
		$this->set($id, $value, $ttl);
		return $value;
	}
	
	private function getFullId($id, $user_specific) {
		$full_id = $id;
		if ($user_specific)
			$full_id .= "#" . $this->ci->authentication->GetUserId();
		return $full_id;
	}
	
	public function set($id, $value, $ttl = self::DEFAULT_TTL, $locations = null) {
		if (!self::CACHING_ON) return;
		
		// Store the value in the CI cache
		$this->ci->cache->save($id, $value, $ttl);
	}
	
	public function load_view($view, $vars, $user_specific = true, $return = false, $ttl = self::DEFAULT_TTL) {
		$key = "view.";
		$key .= str_replace("/", ".", $view);
		foreach ($vars as $var) {
			$key .= "." . urlencode(serialize($var));
		}
		$key = $this->getFullId($key, $user_specific);
		
		$ci = $this->ci;
		return $this->get($key, function() use ($return, $ci, $vars, $view) {
			$value = $ci->load->view($view, $vars, true);
			if (!$return) $ci->output->append_output($value);
			return $value;
		}, $ttl);
	}
}
