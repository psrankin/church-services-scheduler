<?php

class Authentication {
	/**
	 * Retain a reference to the CodeIngiter instance
	 *
	 * @var CI_Controller 
	 */
	private $ci;
	
	/**
	 * Whether the user is logged in
	 *
	 * @var boolean
	 */
	private $is_logged_in;
	
	/**
	 * The ID of the currently logged in user
	 *
	 * @var integer 
	 */
	private $user_id = null;
	
	
	/**************************************************************************/
	/*                             Public Methods                             */
	/**************************************************************************/
	/**
	 * Get the ID of the currently logged in user
	 * 
	 * @return integer The ID of the currently logged in user
	 */
	public function GetUserId() {
		return $this->user_id;
	}
	
	/**
	 * Constructur, construct all authentication data as necessary
	 */
	public function __construct() {
		$this->ci =& get_instance();
		$this->ci->benchmark->mark("Libraries.Authentication.Construct_start");
		$this->ci->load->library("session");
		$this->Load();
		$this->ci->benchmark->mark("Libraries.Authentication.Construct_end");
	}
	
	/**
	 * Attempt to authenticate a particular user given a username/password
	 * 
	 * @param type $username
	 * @param type $password
	 * @return integer The ID of the authenticated user if successful, or null otherwise
	 */
	public function TryAuthenticate($username, $password) {
		$this->ci->load->database();
		$query = $this->ci->db->query("Select Id, PasswordHashed From `User` Where Email = ?;", array($username));
		
		if (($row = $query->result()) && ($row = $row[0])) {
			if (password_verify($password, $row->PasswordHashed)) {
				return $row->Id;
			}
		}

		return null;
	}
	
	
	/**
	 * Mark a particular user as authenticated, and set up the authentication ticket
	 * 
	 * @param integer $user_id The ID of the user to authenticate
	 */
	public function GrantAccessAs($user_id) {
		// Generate a random ticket ID
		$this->ci->load->library(array("encryption", "doctrine"));
		$ticket_id = $user_id . "." . base64_encode($this->ci->encryption->create_key(32));
		
		// Create the ticket and store it into the database
		require_once(APPPATH.'models/Entity/Ticket.php');
		$ticket = new \Entity\Ticket();
		$ticket->setId($ticket_id);
		$ticket->setUser($this->ci->doctrine->em->getPartialReference("\Entity\User", $user_id));
		$ticket->setUserIP($_SERVER['REMOTE_ADDR']);
		$expires = new \DateTime();
		$expires = $expires->add(date_interval_create_from_date_string("+90 days"));
		$ticket->setExpires($expires);
		$ticket->setRevoked(false);
		$this->ci->doctrine->em->persist($ticket);
		$this->ci->doctrine->em->flush($ticket);
		set_cookie("ticket_id", $ticket_id, 129600);
		
		$this->is_logged_in = true;
		$this->EnsureValidCookies();
		
		// Set the current user ID
		$this->SetUserId($user_id);
	}
	
	
	/**
	 * Log the current user out
	 */
	public function LogOut() {
		$user_id = $this->GetUserId();
		if ($user_id !== null) {
			$this->ci->load->database();
			$this->ci->db->query("Update Ticket Set Revoked = 1 Where UserId = ?;", array($user_id));
			$this->ci->load->helper("cookie");
			delete_cookie("ticket_id");
			delete_cookie("is_logged_in");
			
			unset($_SESSION["user_id"]);
			$this->user_id = null;
			
			// Redirect to the login screen
			$this->ci->load->helper("url");
			redirect("/my-account/login");
		}
	}
	
	
	/**
	 * Determine whether the current user is logged in
	 * 
	 * @return boolean True if the user is logged in
	 */
	public function IsLoggedIn() {
		return $this->is_logged_in === true;
	}
	
	
	/**************************************************************************/
	/*                             Private Methods                             */
	/**************************************************************************/
	/**
	 * Load the current user's logged-in status and ticket information
	 */
	private function Load() {
		$is_logged_in = false;
		
		if ($this->ci->session->user_id) {
			$this->user_id = $this->ci->session->user_id;
			$is_logged_in = true;
		} else {
			$ticket_id = $this->ci->input->cookie("ticket_id");
			
			if ($ticket_id) {
				$user_id = $this->VerifyTicket($ticket_id);
				if ($user_id) {
					$this->SetUserId($user_id);
					$is_logged_in = true;
				}
			}
		}
		
		$this->is_logged_in = $is_logged_in;
		$this->EnsureValidCookies();
	}
	
	/**
	 * Verify a ticket in the database, making sure it is still active
	 * 
	 * Returns FALSE if the ticket is not valid.
	 * 
	 * @param integer $ticket_id The ID of the ticket to attempt to build from the database
	 * @return integer The ID of the user to whom this ticket belongs
	 */
	private function VerifyTicket($ticket_id) {
		$this->ci->load->library("doctrine");
		$ticket = $this->ci->doctrine->em->find("\Entity\Ticket", $ticket_id); /* @var $ticket \Entity\Ticket */
		
		if ($ticket->getExpires() < new \DateTime()) {
			return null;
		} else {
			$ticket->getUser()->getId();
		}
	}
	
	/**
	 * Given a particular user ID, generate a random ticket ID for this user
	 * 
	 * @param integer $user_id The ID of the user for whom to generate a ticket ID
	 */
	private function GetTicketId($user_id) {
		$length = 25;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		
		return "$user_id.$randomString";
	}
	
	/**
	 * Set the ID for the current user
	 * 
	 * @param integer $user_id The ID of the current user
	 */
	private function SetUserId($user_id) {
		$_SESSION["user_id"] = $user_id;
		$this->user_id = $user_id;
	}
	
	
	/**
	 * Make sure that the cookies are up-to-date, depending on the user's
	 * logged-in status
	 */
	private function EnsureValidCookies() {
		$this->ci->load->helper("cookie");
		
		if ($this->is_logged_in) {
			if (get_cookie("is_logged_in") != 1) {
				set_cookie("is_logged_in", true, 60);
			}
		} else {
			if (get_cookie("is_logged_in")) {
				delete_cookie ("is_logged_in");
			}
		}
	}
}
