<?php

class Authorization {
	/**************************************************************************/
	/*                          P R O P E R T I E S                           */
	/**************************************************************************/
	/**
	 * The ID of the currently authenticated user
	 *
	 * @var integer 
	 */
	public $user_id;
	
	/**
	 * The ID of the active church for the currently logged in user
	 */
	private $church_id;
	
	/**
	 * A list of global roles to which this user belongs
	 * 
	 * @var array An array of role names that the currently 
	 */
	private $roles;
	
	/**
	 * @var boolean Whether the currently logged in user is a super admin
	 */
	private $is_super_admin = false;
	
	/**
	 * An associative array of role names to privilege names, for all roles
	 *
	 * @var array 
	 * 
	 * E.g.,
	 *   ["admin" => ["edit_page", "manage_users", ...], "editor" => [...]]
	 * 
	 */
	private $role_privileges = null;
	
	/**
	 * Get an associative array of role names to privilege names, for all roles
	 *
	 * @return array
	 * 
	 * E.g.,
	 *   ["admin" => ["edit_page", "manage_users", ...], "editor" => [...]]
	 */
	public function getRolePrivileges() {
		if ($this->role_privileges === null) {
			$this->ci->load->library("cache2");
			$this->role_privileges = $this->ci->cache2->get("role_privileges", function() {
				return $this->GetRolePrivilegesArray();
			}, 60);
		}
		
		return $this->role_privileges;
	}
	
	/**
	 * Access CodeIgniter methods
	 *
	 * @var CI_Controller 
	 */
	private $ci;
	
	/**************************************************************************/
	/*                             Public Methods                             */
	/**************************************************************************/
	public function __construct() {
		$this->ci =& get_instance();
		$this->ci->load->library("authentication");
		$this->user_id = $this->ci->authentication->GetUserId();
		
		// Load the ticket information
		if (!$this->user_id) {
			$this->roles = array();
		} else {
			$this->ci->load->library("cache2");
			$a = $this;
			$authorization = $this->ci->cache2->get("authorization.{$this->user_id}", function() use ($a) {
				return $a->GetUserAuthorization($a->user_id);
			}, 60);

			$this->roles			= $authorization["roles"];
			$this->is_super_admin	= $authorization["is_super_admin"];
		}
	}
	
	/**
	 * Get the ID of the active church for the currently logged in user
	 */
	public function getCurrentChurchId() {
		if (!$this->church_id) {
			if (count($this->roles) < 1) {
				return null;
			}
			
			$this->church_id = array_keys($this->roles)[0];
		}
		
		return $this->church_id;
	}
	
	/**
	 * Get the current church partial reference using Doctrine
	 */
	public function getCurrentChurchPartial() {
		$this->ci->load->library("doctrine");
		return $this->ci->doctrine->em->getPartialReference("Entity\Church", $this->getCurrentChurchId());
	}
	
	/**
	 * Get the current church partial reference using Doctrine
	 * @return Entity\Church
	 */
	public function getCurrentChurch() {
		$this->ci->load->library("doctrine");
		return $this->ci->doctrine->em->find("Entity\Church", $this->getCurrentChurchId());
	}
	
	/**
	 * Get the authorization information for a particular user
	 * 
	 * @param integer $user_id The ID of the user for whom to retrieve authorization information
	 */
	public function GetUserAuthorization($user_id) {
		$this->ci->load->library("doctrine");
		$users = $this->ci->doctrine->em->createQuery("Select u, cur From \Entity\User u Join u.ChurchRoles cur Where u.Id = ?0")
			->execute([$user_id]);
		if (isset($users[0])) {
			$user = $users[0]; /* @var $user Entity\User */
		} else {
			$this->ci->authentication->LogOut();
			die();
		}
		
		// Loop processing all church user roles, adding them to the correct array
		$roles = array();
		foreach ($user->getChurchRoles() as $church_user_role) { /* @var $church_user_role Entity\ChurchUserRole */
			if (!isset($roles[$church_user_role->getChurch()->getId()]))
				$roles[$church_user_role->getChurch()->getId()] = array();
			array_push($roles[$church_user_role->getChurch()->getId()], $church_user_role->getRole()->getName());
		}
		
		return array(
			"roles"				=> $roles,
			"is_super_admin"	=> $user->getIsSuperAdmin()
		);
	}
	
	/**
	 * Determine whether the current viewer is logged in
	 * 
	 * @return boolean
	 */
	public function IsLoggedIn() {
		return $this->user_id ? true : false;
	}
	
	/**
	 * Require the currently authenticated user to be a super-administrator to continue
	 */
	public function RequiresSuperAdmin() {
		if ($this->is_super_admin !== true) {
			$this->AccessDenied();
		}
	}
	
	/**
	 * Deny access to the current user for the current page
	 */
	public function AccessDenied() {
		if (!$this->IsLoggedIn()) {
			$this->ci->output->set_output("");
			$this->ci->load->library("session");
			$this->ci->load->helper("url");
			$_SESSION['login_redirect'] = [
				"Url"			=> current_url(),
				"Timestamp"		=> (new \DateTime())->getTimestamp()
			];
			redirect("/my-account/login");
		} else {
			show_error("You are not authorized to view this content.", 401);
		}
		die();
	}
	
	/**
	 * Verify that the currently authenticated user has the specified privilege
	 * for the specified page.  If not, block access.
	 * 
	 * @param string $privilege_name The name of the privilege for which to check
	 * @param integer $church_id The ID of the church for which access is required
	 */
	public function RequiresPrivilege($privilege_name, $church_id = null) {
		if ($this->HasPrivilege($privilege_name, $church_id) !== true) {
			$this->AccessDenied();
		}
	}
	
	/**
	 * Verify that the current user is logged in
	 */
	public function RequiresLogin() {
		if (!$this->IsLoggedIn()) {
			$this->AccessDenied();
		}
	}
	
	/**
	 * Alias of HasPrivilege
	 * 
	 * @param string $privilege The privilege to check
	 * @return boolean True if the current user has the specified privilege; false otherwise
	 */
	public function Can($privilege, $church_id = null) {
		return $this->HasPrivilege($privilege, $church_id);
	}
	
	/**
	 * Determine whether the currently logged in user has a particular global privilege
	 * 
	 * @param string $privilege The name of the global privilege for which to check
	 * @return boolean True if the user has the specified privilege
	 */
	public function HasPrivilege($privilege, $church_id = null) {
		if ($church_id === null) $church_id = $this->getCurrentChurchId();
		if (!isset($this->roles[$church_id])) return false;
		
		// Loop processing all global roles for the current user, checking 
		// whether the specified privilege is included
		foreach ($this->roles[$church_id] as $role) {
			if ($this->PrivilegeIsInRole($role, $privilege))
				return true;
		}
		
		return false;
	}
	
	
	/**
	 * Get the global privileges for the currently logged in user
	 * 
	 * @return array An array of global privileges for this user
	 */
	public function Privileges() {
		$privileges = array();
		
		// Loop processing all authorized churches, creating privileges arrays for each one
		$role_privileges = $this->getRolePrivileges();
		foreach ($this->roles as $church_id => $roles) {
			$privileges[$church_id] = array();
			
			// Loop processing each role for this particular church, expanding it to a privileges array
			foreach ($roles as $role) {
				foreach ($role_privileges[$role] as $privilege) {
					if (!in_array($privilege, $privileges[$church_id])) {
						array_push($privileges[$church_id], $privilege);
					}
				}
			}
		}
		
		return $privileges;
	}
	
	/**************************************************************************/
	/*                            Private Methods                             */
	/**************************************************************************/
	private function PrivilegeIsInRole($role_name, $privilege_name) {
		if (!$role_name) return false;
		$role_privileges = $this->getRolePrivileges();
		return in_array($privilege_name, $role_privileges[$role_name]);
	}
	
	/**
	 * Build an associative array of roles mapped to privileges
	 * 
	 * @return array
	 * 
	 * E.g.,
	 *   ["admin" => ["page_edit", "users_manage", ...], "editor" => [...]]
	 */
	private function GetRolePrivilegesArray() {
		$role_privileges = array();
		
		$this->ci->load->library("doctrine");
		$query = $this->ci->doctrine->em->createQuery("Select r, p From \Entity\Role r Join r.Privileges p");
		$roles = $query->execute();
		
		// Loop processing each role, adding it and its privileges into the array
		foreach ($roles as $role) { /* @var $role \Entity\Role */
			$role_privileges[$role->getName()] = array();
			
			// Loop processing each privilege within this role, adding it to the array
			foreach ($role->getPrivileges() as $privilege) { /* @var $privilege \Entity\Privilege */
				array_push($role_privileges[$role->getName()], $privilege->getName());
			}
		}
		
		return $role_privileges;
	}
}
