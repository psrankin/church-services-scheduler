<?php

/**
 * Verifies that a ticket is valid, and if not, shuts down the program immediately
 * 
 * @param string $ticket The ticket provided by the user
 * @param string $action The action (URL) expected for this ticket
 * @return Entity\LinkTicket The verified link ticket object
 */
function verify_ticket($ticket, $action) {
	$ci =& get_instance(); /* @var $ci CI_Controller */
	$link_ticket = $ci->doctrine->em->getRepository("Entity\LinkTicket")->findOneBy(['Ticket' => $ticket]); /* @var $link_ticket Entity\LinkTicket */
	if (!$link_ticket || !$link_ticket->Verify($action)) {
		show_error("This link is either invalid or expired.");
		die();
	}
	return $link_ticket;
}


/**
 * Get a Entity\LinkTicket object
 * 
 * Note: This does _not_ flush Doctrine's EM or send an e-mail.
 * 
 * @param string $action The action that this link ticket can perform
 * @param string $subject The subject of the ticket e-mail
 * @param string $message The body of the e-mail to send
 * @param array $data An array of data to serialize into the LinkTicket
 * @param string $expires Something accepted by PHP's strtotime function
 * @return Entity\LinkTicket The link ticket which has been created
 */
function create_link_ticket($action, $subject, $message, $data = [], $expires_relative = "+2 days") {
	$ci =& get_instance();
	
	$ci->doctrine->em->getRepository("Entity\LinkTicket");
	$link_ticket = new \Entity\LinkTicket();
	$ci->doctrine->em->persist($link_ticket);
	$link_ticket->setAction($action);
	$link_ticket->setData(serialize($data));
	$expires = new \DateTime();
	$expires->setTimestamp(strtotime($expires_relative));
	$link_ticket->setExpires($expires);
	$link_ticket->setMessage($message);
	$link_ticket->setSubject($subject);
	$link_ticket->generateTicket();
	$link_ticket->setIsActive(true);
	
	return $link_ticket;
}

/**
 * Sanitize an identifier
 * 
 * @param string $dirty The dirty identifier
 * @return string The cleaned identifier
 */
function clean_identifier($dirty) {
	return preg_replace("/[^A-Za-z0-9_]/", "", $dirty);
}

/**
 * Generate a random string, using a cryptographically secure 
 * pseudorandom number generator (random_int)
 * 
 * For PHP 7, random_int is a PHP core function
 * For PHP 5.x, depends on https://github.com/paragonie/random_compat
 * 
 * @param int $length      How many characters do we want?
 * @param string $keyspace A string of all possible characters
 *                         to select from
 * @return string
 */
function randomString($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
    $str = '';
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $str .= $keyspace[random_int(0, $max)];
    }
    return $str;
}
