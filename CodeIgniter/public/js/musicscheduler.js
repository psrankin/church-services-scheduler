$(function () {
    var relativeUrl = window.location.pathname;
    $("ul.nav a").filter(function () {
        return $(this).attr("active-regex") && new RegExp($(this).attr("active-regex")).test(relativeUrl);
    }).addClass("active");
});