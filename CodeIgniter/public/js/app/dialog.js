var MODAL_TEMPLATE = null;

function Dialog() {
    var self = this;
    
    /**
     * The dialog HTML DOM jQuery object being displayed
     */
    self.dialog = null;
    
    /**
     * The buttons to display.  Each element has the following keys:
     *   Title, Callback, Css
     */
    self.buttons = [];
    
    /**
     * Remember the URL for this form (for follow-up POST requests)
     */
    self.url = null;
    
    /**
     * Add a button to the form
     * 
     * @param {string} title The title/text of the button to add
     * @param {callable} callback The callback to execute if the user clicks
     *   this button
     * @returns {Dialog}
     */
    self.addButton = function(title, css, callback) {
        self.buttons.push({Title: title, Callback: callback, Css: css});
        return self;
    };
    
    /**
     * Add the Save button automatically to this dialog.  It will take care
     * of posting the data back to the server, checking for validation errors, 
     * etc.
     * 
     * @returns {Dialog}
     */
    self.addSaveButton = function() {
        return self.addButton("Save Changes", "btn btn-primary", function() {
            self.dialog.find(".SaveChanges").attr("disabled", "disabled").html("Saving...");
            $.ajax({
                url: self.url,
                method: "POST",
                data: self.dialog.find("form").serialize()
            }).done(function(data) {
                if (!data || typeof(data) == "object") {
                    self.dialog.modal('hide').remove();
                    def.resolveWith(this, data);
                } else {
                    self.dialog.find(".modal-body").html(data["Results"]);
                    self.dialog.find(".modal-body").removeAttr("disabled");
                }
            });
        });
    };
    
    /**
     * Asynchronously fetch the HTML template for a generic dialog from the server
     * 
     * @returns {Dialog.getTemplate.def|Deferred}
     */
    function getTemplate() {
        var def = $.Deferred();
        
        // Check the cached dialog HTML.  If it doesn't exist, create it.
        if (MODAL_TEMPLATE === null) {
            $.ajax({
                url: '/public/js/app/modal_template.html'
            }).done(function(data) {
                MODAL_TEMPLATE = data;
                def.resolveWith(this, [MODAL_TEMPLATE]);
            });
        } else {
            def.resolveWith(this, [MODAL_TEMPLATE]);
        }
        
        return def;
    }
    
    self.show = function(title, url) {
        var def = $.Deferred();
        
        self.url = url;
        
        getTemplate().done(function(template) {
            var dialog = $(template);
            dialog.find(".modal-header h4").html(title);
            
            // Attach event listeners as required
            dialog.on('hidden.bs.modal', function (e) {
                self.dialog.modal('hide').remove();
                def.resolve();
            });
            
            // Add buttons and attach event listeners
            $(self.buttons).each(function(i, v) {
                $("<button type='button'></button>").addClass(v.Css).html(v.Title)
                    .click(v.Callback).appendTo(dialog.find(".modal-footer"));
            });
            
            $.ajax({
                url: url
            }).done(function(data) {
                dialog.find(".modal-body").html(data);
                $("body").append(dialog);
                self.dialog = $("#MyModal");
                self.dialog.modal();
            });
        });
        
        return def;
    }
    
    self.hide = function() {
        self.dialog.modal('hide').remove();
    }
}