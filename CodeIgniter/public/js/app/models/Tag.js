function Tag(data) {
    var self = this;
    
    self.Id         = data.Id;
    self.ChurchId   = data.ChurchId;
    self.Category   = data.Category;
    self.Title      = data.Title;
    self.Details    = data.Details;
}
