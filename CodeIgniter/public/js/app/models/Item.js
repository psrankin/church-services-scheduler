function Item(data) {
    var self = this;
    
    self.Id                 = data.Id;
    self.ChurchId           = data.ChurchId;
    self.Type               = ko.observable(data.Type);
    self.Title              = ko.observable(data.Title);
    self.Summary            = ko.observable(data.Summary);
    self.Length             = ko.observable(data.Length);
    self.LengthNormalized   = ko.observable(data.LengthNormalized);
    self.Key                = ko.observable(data.Key);
    self.Page               = ko.observable(data.Page);
    self.Details            = ko.observable(data.Details);
    self.Tags               = ko.observableArray($.map(function(tag) { return new Tag(tag); }));
    self.LastPerformed      = data.LastPerformed;
    self.LastPerformedHere  = data.LastPerformedHere;
    
    self.allTagTitles       = ko.computed(function() { return  $.map(function(tag) { return tag.Title(); }).join(", "); });
    self.isHymn             = ko.computed(function() { return self.Type() === "Hymn"; });
    self.isCongregational   = ko.computed(function() { return self.isHymn() || self.Type() === "Congregational"; });
    self.isSpecial          = ko.computed(function() { return self.Type() === "Special"; });
    self.isReading          = ko.computed(function() { return self.Type() === "Reading"; });
    
    self.FullTitle = ko.computed(function() {
        var fullTitle = self.Title();
        if (self.Page()) fullTitle = "#" + self.Page() + ": " + fullTitle;
        if (self.Key()) fullTitle += " (" + self.Key() + ")";
        if (self.Type() === "Sermon") fullTitle = "Sermon: " + fullTitle;
        return fullTitle;
    });
}
