function ServiceItem(data) {
    var self = this;
    
    self.Id             = data.Id;
    self.Item           = ko.observable(data.Item ? new Item(data.Item) : null);
    self.Weight         = ko.observable(data.Weight);
}
