var ItemListViewModel = function (serviceId) {
    // Data
    var self = this;
    self.Items = ko.observableArray([]);
    self.Search = ko.observable('');
    self.FilteredItems = ko.computed(function () {
        var searchWords = self.Search().toLowerCase().split(/[^A-Za-z0-9']/i);
        
        return $(self.Items()).filter(function(i, v) {
            var searchText = (v.Title() + " " + v.Summary() + " " + v.allTagTitles()).toLowerCase();
            
            // Make sure each search word is present
            for (var i = 0; i < searchWords.length; i++) {
                if (searchText.indexOf(searchWords[i]) === -1)
                    return false;
            }
            
            return true;
        });
    });
    
    self.IncludeHymns = ko.observable();
    self.IncludeCongregationals = ko.observable();
    self.IncludeSpecials = ko.observable();
    self.IncludeReadings = ko.observable();
    
    // Operations
    self.addItemToProgram = function(Item) {

    }
    
    $.getJSON("/items/all/" + serviceId, function(data) {
        self.Items($.map(data, function(item) { return new Item(item); }));
    });
}