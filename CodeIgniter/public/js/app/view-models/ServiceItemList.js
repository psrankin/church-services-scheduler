var ServiceItemListViewModel = function (serviceId, serviceItems) {
    var self = this;
    var serviceId = serviceId;
    var ServiceItemsSelector = "#ServiceItems";
    
    // Data
    var self = this;
    self.ServiceItems = ko.observableArray($.map(serviceItems, function(x) { return new ServiceItem(x); }));
    
    $(function() {
       self.addSortable();
    });
    
    self.addSortable = function() {
        $(ServiceItemsSelector).sortable({ 
            handle: ".Handle", 
            stop: function(e, i) {
                $.post("/service-items/reorder/" + serviceId, {
                        ServiceItemIds: JSON.stringify($("#ServiceItems .ServiceItem").map(function(i, x) { return $(x).attr("entityid"); }).get())
                    }
                );
            }
        });
    };
    
    // Operations
    self.addItemToProgram = function(Item) {
        
    }
    
    self.addServiceItem = function(item) {
        $(ServiceItemsSelector).sortable('destroy');
        self.ServiceItems.push(item);
        self.addSortable();
    };
}