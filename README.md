# Church Services Scheduler

Main website: http://www.christianprogrammers.com/church/

Forums: http://forum.christianprogrammers.com/forumdisplay.php?fid=8

Bug tracker/project management: http://mantis.christianprogrammers.com/set_project.php?project_id=1

## Overview

This project is a web-based application written in PHP.  Main technologies used on the backend include [CodeIgniter](http://www.codeigniter.com/) MVC framework, and [Doctrine ORM](http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/).  Main technologies on the front-end include [Bootstrap](http://getbootstrap.com/getting-started/), [jQueryUI](http://jqueryui.com/) and some [KnockoutJS](http://knockoutjs.com/).

To contribute, you can visit the forums or the project management site (links given above).