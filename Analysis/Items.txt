Service components:

  - Congregational/Hymn
    - Hymnal?
    - Number
    - Title
    - Verses (Roadmap)
    
    Auto-suggest hymns and hymnals as possible
    
  - Special Music
    - Who
    - Title
    
    * Can carry over to different services
    
  - Sermon
    - Length
    - Person
    - [Title="Sermon"]
    
    * Not linked to any particular item
  
  - Special Reading
    - Persons
  
  - Other
    - Title
    - Details
    
    * Not linked to any particular item
  
  
  